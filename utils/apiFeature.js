const _ = require('lodash');

class ApiFeature {
  constructor(query, queryString) {
    this.query = query;
    this.queryString = queryString;
  }

  searchByCurrentUser(userID, field) {
    const exclude = ['limit', 'page', 'sort', 'fields'];

    if (field) exclude.push(field);

    let queryString = _.omit(this.queryString, exclude); //Exclusive fields for others action

    if (field) {
      queryString[field] = userID;
    }

    queryString = JSON.stringify(queryString);

    queryString = queryString.replace(/(lt|lte|gt|gte)/, match => `$${match}`);

    this.query = this.query.find(JSON.parse(queryString));

    return this;
  }

  search() {
    let queryString = _.omit(this.queryString, [
      'limit',
      'page',
      'sort',
      'fields'
    ]); //Exclusive fields for others action

    queryString = JSON.stringify(queryString);

    queryString = queryString.replace(/(lt|lte|gt|gte)/, match => `$${match}`);

    this.query = this.query.find(JSON.parse(queryString));

    return this;
  }

  limitFields() {
    if (this.queryString.fields) {
      const selectedField = this.queryString.fields.split(',').join(' ');
      this.query.select(selectedField);
    } else {
      this.query.select('-__v');
    }

    return this;
  }

  sort() {
    if (this.queryString.sort) {
      const search = this.queryString.sort.split(',').join(' ');
      this.query.sort(search);
    } else {
      this.query.sort('-createdAt');
    }

    return this;
  }

  pagination() {
    const page = this.queryString.page * 1 || 1;
    const limit = this.queryString.limit * 1 || 10; //Setting per page
    const skip = (page - 1) * limit;
    this.query.skip(skip).limit(limit);
    return this;
  }
}

module.exports = ApiFeature;
