const catchAsync = fn => {
  return (req, res, next) => {
    fn(req, res, next).catch(next); //Next 4 params
  };
};

module.exports = catchAsync;
