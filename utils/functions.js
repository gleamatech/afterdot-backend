const passport = require('passport')

export const resError = (msg, res, code = 500) => {
    res.status(code).json({
        status: false,
        msg
    })
}

export const resOk = (data, res, code = 200) => {
    res.status(code).json({
        status: true,
        data
    })
}


export const authenticate = () => {
    return passport.authenticate(['jwt'], { session: false})
}