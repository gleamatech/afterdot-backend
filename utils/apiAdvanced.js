const _ = require('lodash');

class apiAdvanced {
  constructor(model, query, refObj) {
    this.model = model;
    this.query = query;
    this.ref = refObj;
  }

  search() {
    let queryString = _.omit(this.query, ['limit', 'page', 'sort', 'fields']); //Exclusive fields for others action

    queryString = JSON.stringify(queryString);

    queryString = queryString.replace(/(lt|lte|gt|gte)/, match => `$${match}`);

    this.match = JSON.parse(queryString);

    return this;
  }

  sort() {
    const s = {};
    if (this.query.sort) {
      const search = this.queryString.sort.split(',');
      search.map(v => {
        const tmp = v.replace('-', '');
        if (v.indexOf('-') >= 0) {
          s[tmp] = -1;
        } else {
          s[tmp] = 1;
        }
        return 1;
      });
    }

    this.sort = _.values(s).length > 0 ? s : null;

    return this;
  }

  pagination() {
    const page = this.query.page * 1 || 1;
    this.limit = this.query.limit * 1 || 10; //Setting per page
    this.skip = (page - 1) * this.limit;
    return this;
  }

  finish() {
    const agg = [
      {
        $lookup: {
          from: this.ref.from,
          localField: this.ref.local,
          foreignField: this.ref.foreign,
          as: this.ref.as
        }
      }
    ];

    if (this.match) agg.push({ $match: { ...this.match } });
    if (this.sort) agg.push({ $sort: { ...this.sort } });
    if (this.skip) agg.push({ $skip: this.skip });
    if (this.limit) agg.push({ $limit: this.limit });

    return this.model.aggregate(agg);
  }
}

module.exports = apiAdvanced;
