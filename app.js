const express = require('express');
const app = express();
const helmet = require('helmet');
const xssClean = require('xss-clean');
const cors = require('cors');
const passportConfig = require('./config/passport')
const passport = require('passport');
const rateLimit = require('express-rate-limit');
const routes = require('./routes/index');

const limiter = rateLimit({
  windowMs: 60 * 1000, // 15 minutes
  max: 1000 // limit each IP to 100 requests per windowMs
});

passport.use(passportConfig)

app.use(limiter);

// app.use(morgan('combined'));

app.use(passport.initialize());

app.use(cors()); //Prevent Cors

app.use(helmet()); //Prevent Inject

app.use(xssClean()); //Prevent XSS Inject

app.use(express.json({ limit: '50mb' })); //Support application/json

// //firebase
// var serviceAccount = require("./firebaseTest.json");

// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount)
// });

//Routing

app.use('/api', routes);

app.use('/public', express.static('public'));

//Otherwise
app.all('*', (req, res, next) => {
  res.send('404 Not Found');
});

// Run cronjob
module.exports = app;
