const JwtStrategy = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
// const jwt = require('jsonwebtoken');
const User = require('../models/userModel');

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET
}

const passportConfig = new JwtStrategy(jwtOptions, async (payload, next) => {
    const { userId } = payload
    const user = await User.findById(userId)

    if (user) {
     return next(null, user)
    }

     next(null, false)
})


module.exports = passportConfig