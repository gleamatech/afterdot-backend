require('dotenv').config();

const http = require('http');

const mongoose = require('mongoose');

const app = require('./app');

const PORT = process.env.PORT || 3333;

mongoose
  .connect(process.env.DB_STRING, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log('Database is connected');
  })
  .catch(e => console.log(e));

const server = http.createServer(app).listen(PORT, () => {
  console.log(`Server listening on ${PORT}`);
});

process.on('uncaughtException', function() {
  console.log('Server is shutting down');
  server.close(e => {
    if (!e) console.log('Can not shut down server check: ', e);
    console.log('Server is totally shutdown');
  });
});
