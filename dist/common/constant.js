"use strict";

exports.ROOM_EVENT = {
  GET_ALL_ROOM: 'GET_ALL_ROOM',
  //Send all room
  JOIN_ROOM: 'JOIN_ROOM',
  //Some one join room,
  CLOSE_ROOM: 'CLOSE_ROOM',
  NEW_ROOM: 'NEW_ROOM',
  HOLD_ROOM: 'HOLD_ROOM',
  LEAVE_ROOM: 'LEAVE_ROOM',
  JOIN_ROOM_SUCCESS: 'JOIN_ROOM_SUCCESS',
  ERROR: 'ERROR'
};
exports.ROOM_PACKAGE = [5, 7, 8, 9, 10, 11, 12, 13, 14, 15, 18];
exports.MEGA_PRICE = [{
  bao: 5,
  price: 400
}, {
  bao: 7,
  price: 70
}, {
  bao: 8,
  price: 280
}, {
  bao: 9,
  price: 840
}, {
  bao: 10,
  price: 2100
}, {
  bao: 11,
  price: 4620
}, {
  bao: 12,
  price: 9240
}, {
  bao: 13,
  price: 17160
}, {
  bao: 14,
  price: 30030
}, {
  bao: 15,
  price: 50050
}, {
  bao: 18,
  price: 185640
}];
exports.POWER_PRICE = [{
  bao: 5,
  price: 500
}, {
  bao: 7,
  price: 70
}, {
  bao: 8,
  price: 280
}, {
  bao: 9,
  price: 840
}, {
  bao: 10,
  price: 2100
}, {
  bao: 11,
  price: 4620
}, {
  bao: 12,
  price: 9240
}, {
  bao: 13,
  price: 17160
}, {
  bao: 14,
  price: 30030
}, {
  bao: 15,
  price: 50050
}, {
  bao: 18,
  price: 185640
}];
exports.SUB_GAME = [{
  display: 'Lớn',
  value: 'lon',
  description: 'Có 11 số trở lên từ 41 đến 80'
}, {
  display: 'Hoà lớn nhỏ',
  value: 'hoa-lonnho',
  description: 'Có 10 số từ 01 đến 40 và 10 số từ 41 đến 80'
}, {
  display: 'Nhỏ',
  value: 'nho',
  description: 'Có 11 số từ 01 đến 40'
}, {
  display: 'Chẵn',
  value: 'chan',
  description: 'Có 13 số trở lên là số chẵn'
}, {
  display: 'Chẵn 11-12',
  value: 'chan-11-12',
  description: 'Có 11 hoặc 12 số chẵn'
}, {
  display: 'Hoà chẵn lẻ',
  value: 'hoa-chanle',
  description: 'Có 10 số chẵn và 10 số lẻ'
}, {
  display: 'Lẻ 11-12',
  value: 'le-11-12',
  description: 'Có 11 hoặc 12 số lẻ'
}, {
  display: 'Lẻ',
  value: 'le',
  description: 'Có 13 số lẻ trở lên'
}];
exports.MAX_4D = [{
  index: 1,
  name: 'Max 4D'
}, {
  index: 2,
  name: 'Tổ hợp'
}, {
  index: 3,
  name: 'Bao'
}, {
  index: 4,
  name: 'Cuộn'
}, {
  index: 5,
  name: 'Cuộn 4'
}];