"use strict";

var express = require('express');

var app = express();

var helmet = require('helmet');

var xssClean = require('xss-clean');

var cors = require('cors');

var passport = require('passport'); // const Sentry = require('@sentry/node');


var rateLimit = require('express-rate-limit');

var morgan = require('morgan');

var routes = require("./routes/index"); // Init sentry
// Sentry.init({
//   dsn:
//     'https://ffdac553ae734e4e8204a905458ff1bc@o414700.ingest.sentry.io/5304649'
// });
// Init firebase


var limiter = rateLimit({
  windowMs: 60 * 1000,
  // 15 minutes
  max: 1000 // limit each IP to 100 requests per windowMs

});
app.use(limiter);
app.use(morgan('combined'));
app.use(passport.initialize());
app.use(cors()); //Prevent Cors

app.use(helmet()); //Prevent Inject

app.use(xssClean()); //Prevent XSS Inject

app.use(express.json({
  limit: '50mb'
})); //Support application/json
//Routing

app.use('/api', routes);
app.use('/public', express["static"]('public')); //Otherwise

app.all('*', function (req, res, next) {
  res.send('404 Not Found');
}); // Run cronjob

module.exports = app;