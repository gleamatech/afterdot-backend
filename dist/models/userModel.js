"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var mongoose = require('mongoose');

var validator = require('validator');

var bcrypt = require('bcryptjs');

var crypto = require('crypto');

var beautifyUnique = require('mongoose-beautiful-unique-validation');

var userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'Vui lòng nhập tên để đăng ký']
  },
  email: {
    type: String,
    lowercase: true,
    validate: {
      validator: validator.isEmail,
      message: 'You must enter correct email format'
    },
    "default": "".concat(new Date().getTime(), "@gmail.com")
  },
  password: {
    type: String,
    required: [true, 'Password is required'],
    min: 8
  },
  passwordConfirm: {
    type: String
  },
  role: {
    type: String,
    "enum": ['admin', 'user', 'vip', 'seller', 'staff', 'agency'],
    "default": 'user'
  },
  phoneNumber: {
    type: String,
    required: [true, 'Vui lòng nhập số điện thoại'],
    unique: 'Số điện thoại đã được đăng ký, vui lòng thử lại'
  },
  balance: {
    type: Number,
    "default": 0
  },
  image: String,
  address: String,
  country: String,
  passwordChangedAt: Date,
  passwordResetToken: String,
  passwordResetExpired: Date,
  createdAt: {
    type: Date,
    "default": Date.now()
  },
  active: {
    type: Boolean,
    "default": true
  },
  agency: {
    type: mongoose.Schema.ObjectId,
    ref: 'Agency'
  },
  token: String,
  fmcToken: String
}, {
  toJSON: {
    virtuals: true
  },
  toObject: {
    virtuals: true
  },
  versionKey: false
});
userSchema.plugin(beautifyUnique);
userSchema.pre('save', /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(next) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            if (this.isModified('password')) {
              _context.next = 2;
              break;
            }

            return _context.abrupt("return", next());

          case 2:
            _context.next = 4;
            return bcrypt.hash(this.password, 12);

          case 4:
            this.password = _context.sent;
            //delete password confirm after has
            this.passwordConfirm = undefined;
            if (!this.isNew) this.passwordChangedAt = Date.now();
            next();

          case 8:
          case "end":
            return _context.stop();
        }
      }
    }, _callee, this);
  }));

  return function (_x) {
    return _ref.apply(this, arguments);
  };
}());
userSchema.pre('save', function (next) {
  if (!this.isModified('password') || this.isNew) return next();
  this.passwordChangedAt = Date.now();
  next();
});

userSchema.methods.checkPassword = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(password, inputPassword) {
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return bcrypt.compare(inputPassword, password);

          case 2:
            return _context2.abrupt("return", _context2.sent);

          case 3:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x2, _x3) {
    return _ref2.apply(this, arguments);
  };
}();

userSchema.methods.createResetPasswordToken = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
  var token;
  return regeneratorRuntime.wrap(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          token = crypto.randomBytes(32).toString('hex');
          this.passwordResetToken = crypto.createHash('sha256').update(token).digest('hex');
          this.passwordResetExpired = Date.now() + 15 * 60 * 1000;
          return _context3.abrupt("return", token);

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  }, _callee3, this);
}));
var User = mongoose.model('User', userSchema);
module.exports = User;