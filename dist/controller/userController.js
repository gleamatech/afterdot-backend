"use strict";

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var _ = require('lodash');

var mongoose = require('mongoose');

var factory = require("./factoryHandler");

var User = require("../models/userModel");

var catchAsync = require("../utils/catchAsync");

var AppError = require("../utils/appError");

var ApiFeature = require("../utils/apiFeature");

var Lottery = require("../models/lotteryModel");

var _require = require("./lotteryController"),
    getResultForUser = _require.getResultForUser;

var Transaction = require("../models/transactionModel");

var _require2 = require("../utils/functions"),
    formatMoney = _require2.formatMoney;

var firebaseServices = require("../services/firebase");

var Notification = require("../models/notificationModel");

exports.getUsers = catchAsync( /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res, next) {
    var f, feature, docs, data;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            f = User.find();
            feature = new ApiFeature(f, req.query).search().sort().limitFields().pagination();
            _context2.next = 4;
            return feature.query;

          case 4:
            docs = _context2.sent;
            data = docs.map( /*#__PURE__*/function () {
              var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(it) {
                var history, result, totalWin, totalWinTicket, totalTicket, maxWin;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return Lottery.find({
                          customer: mongoose.Types.ObjectId(it._id)
                        }).populate('customer', '-password');

                      case 2:
                        history = _context.sent;
                        _context.next = 5;
                        return getResultForUser(history);

                      case 5:
                        result = _context.sent;
                        //Find total win
                        result = result.map(function (item) {
                          var win = item.win.reduce ? item.win.reduce(function (a, b) {
                            return a + b;
                          }, 0) : item.win;
                          return _objectSpread(_objectSpread({}, item), {}, {
                            win: win
                          });
                        });
                        totalWin = result.reduce(function (total, current) {
                          return total + current.win;
                        }, 0);
                        totalWinTicket = _.get(result.filter(function (item) {
                          return item.win > 0;
                        }), 'length', 0);
                        totalTicket = _.get(result, 'length', 0);
                        maxWin = Math.max.apply(Math, _toConsumableArray(result.map(function (item) {
                          return item.win;
                        })));
                        return _context.abrupt("return", _objectSpread(_objectSpread({}, it.toObject()), {}, {
                          totalWin: totalWin,
                          totalTicket: totalTicket,
                          totalWinTicket: totalWinTicket,
                          maxWin: maxWin
                        }));

                      case 12:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee);
              }));

              return function (_x4) {
                return _ref2.apply(this, arguments);
              };
            }());
            _context2.t0 = res.status(200);
            _context2.t1 = docs.length;
            _context2.next = 10;
            return Promise.all(data);

          case 10:
            _context2.t2 = _context2.sent;
            _context2.t3 = {
              docs: _context2.t2
            };
            _context2.t4 = {
              status: true,
              results: _context2.t1,
              data: _context2.t3
            };

            _context2.t0.json.call(_context2.t0, _context2.t4);

          case 14:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x, _x2, _x3) {
    return _ref.apply(this, arguments);
  };
}());
exports.getAllUser = factory.getAll(User);
exports.getUser = factory.getOne(User);
exports.createUser = factory.createOne(User);
exports.deleteUser = factory.deleteOne(User);
exports.updateUser = factory.updateOne(User);

exports.setUserId = function (req, res, next) {
  if (!req.body.userId) req.body.userId = req.user._id;
  next();
};

exports.getMe = catchAsync( /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
    var me;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return User.findById(req.user._id);

          case 2:
            me = _context3.sent;
            res.status(200).json({
              status: true,
              user: _objectSpread({}, _.omit(me, ['password']))
            });

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6, _x7) {
    return _ref3.apply(this, arguments);
  };
}());
exports.updateMe = catchAsync( /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res, next) {
    var _req$body, name, fmcToken, me;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _req$body = req.body, name = _req$body.name, fmcToken = _req$body.fmcToken;
            _context4.next = 3;
            return User.findOne({
              _id: req.user._id
            });

          case 3:
            me = _context4.sent;
            if (name) me.name = name;
            if (fmcToken) me.fmcToken = fmcToken;
            me.save();
            res.status(200).json({
              status: true
            });

          case 8:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x8, _x9, _x10) {
    return _ref4.apply(this, arguments);
  };
}());
exports.changeUserPassword = catchAsync( /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res, next) {
    var user, newPassword, u;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            // const { id } = req.body;
            user = req.body.user;
            newPassword = req.body.newPassword;
            _context5.next = 4;
            return User.findById(user._id);

          case 4:
            u = _context5.sent;

            if (u) {
              _context5.next = 7;
              break;
            }

            return _context5.abrupt("return", next(new AppError('Tài khoản không tồn tại', 400)));

          case 7:
            // const check = await u.checkPassword(u.password, password);
            // if (!check) return next(new AppError('Mật khẩu không chính xác', 400));
            u.password = newPassword;
            u.passwordConfirm = newPassword;
            _context5.next = 11;
            return u.save();

          case 11:
            res.json({
              status: true,
              user: u
            });

          case 12:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x11, _x12, _x13) {
    return _ref5.apply(this, arguments);
  };
}());
exports.changePassword = catchAsync( /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res, next) {
    var id, _req$body2, password, newPassword, u, check;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            id = _.get(req, ['user', 'id']);
            _req$body2 = req.body, password = _req$body2.password, newPassword = _req$body2.newPassword;
            _context6.next = 4;
            return User.findById(id);

          case 4:
            u = _context6.sent;

            if (u) {
              _context6.next = 7;
              break;
            }

            return _context6.abrupt("return", next(new AppError('Email is not exists', 400)));

          case 7:
            _context6.next = 9;
            return u.checkPassword(u.password, password);

          case 9:
            check = _context6.sent;

            if (check) {
              _context6.next = 12;
              break;
            }

            return _context6.abrupt("return", next(new AppError('Mật khẩu không chính xác', 400)));

          case 12:
            u.password = newPassword;
            u.passwordConfirm = newPassword;
            _context6.next = 16;
            return u.save();

          case 16:
            res.json({
              status: true,
              user: u
            });

          case 17:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x14, _x15, _x16) {
    return _ref6.apply(this, arguments);
  };
}());
exports.addCredit = catchAsync( /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res, next) {
    var agency, user, _req$body3, id, amount, findUser, transaction, newBalance, t;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            // Add bang tay
            agency = req.agency, user = req.user;
            _req$body3 = req.body, id = _req$body3.id, amount = _req$body3.amount;
            _context7.next = 4;
            return User.findById(id);

          case 4:
            findUser = _context7.sent;

            if (findUser) {
              _context7.next = 7;
              break;
            }

            return _context7.abrupt("return", res.status(200).json({
              status: false,
              msg: 'Tài khoản không tồn tại'
            }));

          case 7:
            if (!(user.agency !== _.get(agency, '_id') && user.role !== 'admin')) {
              _context7.next = 9;
              break;
            }

            return _context7.abrupt("return", res.status(200).json({
              status: false,
              msg: 'Tài khoản này không tồn tại.'
            }));

          case 9:
            _context7.next = 11;
            return Transaction.create({
              customer: id,
              agency: _.get(agency, '_id'),
              method: 'add',
              amount: parseFloat(amount),
              reason: "\u0110\xE3 n\u1EA1p ".concat(formatMoney(amount), " v\xE0o t\xE0i kho\u1EA3n")
            });

          case 11:
            transaction = _context7.sent;
            newBalance = parseFloat(findUser.balance) + parseFloat(amount);
            t = findUser.toObject();
            t.balance = newBalance;

            if (!t.fmcToken) {
              _context7.next = 19;
              break;
            }

            _context7.next = 18;
            return firebaseServices.messaging().sendToDevice([t.fmcToken], {
              notification: {
                body: "T\xE0i kho\u1EA3n c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c +".concat(formatMoney(amount)),
                title: 'Nạp tiền thành công',
                sound: 'default'
              },
              data: {
                updateType: 'MONEY_CHANGE'
              }
            }, {
              // Required for background/quit data-only messages on iOS
              contentAvailable: true,
              // Required for background/quit data-only messages on Android
              priority: 'high'
            });

          case 18:
            Notification.create({
              type: 'TICKET_UPDATE',
              title: 'Nạp tiền thành công',
              message: "T\xE0i kho\u1EA3n c\u1EE7a b\u1EA1n \u0111\xE3 \u0111\u01B0\u1EE3c +".concat(formatMoney(amount)),
              info: req.params.id,
              user: t._id
            });

          case 19:
            return _context7.abrupt("return", res.json({
              status: true,
              user: t,
              transaction: transaction
            }));

          case 20:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x17, _x18, _x19) {
    return _ref7.apply(this, arguments);
  };
}());
exports.subCredit = catchAsync( /*#__PURE__*/function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res, next) {
    var agency, user, _req$body4, id, amount, reason, findUser, transaction, newBalance, t;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            // Add bang tay
            agency = req.agency, user = req.user;
            _req$body4 = req.body, id = _req$body4.id, amount = _req$body4.amount, reason = _req$body4.reason;
            _context8.next = 4;
            return User.findById(id);

          case 4:
            findUser = _context8.sent;

            if (findUser) {
              _context8.next = 7;
              break;
            }

            return _context8.abrupt("return", res.status(200).json({
              status: false,
              msg: 'Tài khoản không tồn tại'
            }));

          case 7:
            if (!(user.agency !== _.get(agency, '_id') && user.role !== 'admin')) {
              _context8.next = 9;
              break;
            }

            return _context8.abrupt("return", res.status(200).json({
              status: false,
              msg: 'Tài khoản này không tồn tại.'
            }));

          case 9:
            _context8.next = 11;
            return Transaction.create({
              customer: id,
              agency: _.get(agency, '_id'),
              method: 'minus',
              amount: -parseFloat(amount),
              reason: reason || "\u0110\xE3 r\xFAt -".concat(formatMoney(amount))
            });

          case 11:
            transaction = _context8.sent;
            newBalance = parseFloat(findUser.balance) - parseFloat(amount);
            t = findUser.toObject();
            t.balance = newBalance;

            if (!t.fmcToken) {
              _context8.next = 19;
              break;
            }

            _context8.next = 18;
            return firebaseServices.messaging().sendToDevice([t.fmcToken], {
              notification: {
                body: "T\xE0i kho\u1EA3n c\u1EE7a b\u1EA1n \u0111\xE3 r\xFAt ".concat(formatMoney(amount)),
                title: 'Rút tiền thành công',
                sound: 'default'
              },
              data: {
                updateType: 'MONEY_CHANGE'
              }
            }, {
              // Required for background/quit data-only messages on iOS
              contentAvailable: true,
              // Required for background/quit data-only messages on Android
              priority: 'high'
            });

          case 18:
            Notification.create({
              type: 'TICKET_UPDATE',
              title: 'Rút tiền thành công',
              message: "T\xE0i kho\u1EA3n c\u1EE7a b\u1EA1n \u0111\xE3 r\xFAt +".concat(formatMoney(amount)),
              info: req.params.id,
              user: t._id
            });

          case 19:
            return _context8.abrupt("return", res.json({
              status: true,
              user: t,
              transaction: transaction
            }));

          case 20:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x20, _x21, _x22) {
    return _ref8.apply(this, arguments);
  };
}());
exports.getTransaction = catchAsync( /*#__PURE__*/function () {
  var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res, next) {
    var user, f, feature, docs;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            user = req.user;
            f = Transaction.find();
            feature = new ApiFeature(f, req.query).searchByCurrentUser(mongoose.Types.ObjectId(user._id), 'customer').sort().limitFields().pagination();
            _context9.next = 5;
            return feature.query;

          case 5:
            docs = _context9.sent;
            res.status(200).json({
              status: true,
              results: docs.length,
              data: {
                docs: docs
              },
              currentBalance: user.balance
            });

          case 7:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x23, _x24, _x25) {
    return _ref9.apply(this, arguments);
  };
}());
exports.searchByPhone = catchAsync( /*#__PURE__*/function () {
  var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res, next) {
    var phoneNumber, f, feature, total, findFeature, docs;
    return regeneratorRuntime.wrap(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            phoneNumber = req.body.phoneNumber;
            f = User.find({
              phoneNumber: phoneNumber
            });
            feature = new ApiFeature(f, req.query).search();
            _context10.next = 5;
            return feature.query.count();

          case 5:
            total = _context10.sent;
            findFeature = new ApiFeature(f, req.query).search().sort().limitFields().pagination();
            _context10.next = 9;
            return findFeature.query;

          case 9:
            docs = _context10.sent;
            res.status(200).json({
              status: true,
              results: docs.length,
              data: {
                docs: docs
              },
              total: total,
              page: req.query.page || 1,
              limit: req.query.limit || 10
            });

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, _callee10);
  }));

  return function (_x26, _x27, _x28) {
    return _ref10.apply(this, arguments);
  };
}());

exports.addByBank = function () {};