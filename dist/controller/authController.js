"use strict";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var jwt = require('jsonwebtoken');

var _ = require('lodash');

var crypto = require('crypto');

var _require = require('util'),
    promisify = _require.promisify;

var bcrypt = require('bcryptjs');

var google = require('googleapis');

var User = require("../models/userModel");

var AppError = require("../utils/appError");

var catchAsync = require("../utils/catchAsync");

var Agency = require("../models/agencyModel");

var firebaseServices = require("../services/firebase");

var secret = process.env.JWT_SECRET;

var signToken = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(userId) {
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return jwt.sign({
              userId: userId
            }, secret, {
              expiresIn: '90d'
            });

          case 2:
            return _context.abrupt("return", _context.sent);

          case 3:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function signToken(_x) {
    return _ref.apply(this, arguments);
  };
}();

var sendToken = /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(user, statusCode, res) {
    var token, cookieOpt;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return signToken(user._id);

          case 2:
            token = _context2.sent;
            _context2.next = 5;
            return User.findByIdAndUpdate(user._id, {
              token: token
            });

          case 5:
            cookieOpt = {
              expires: new Date(Date.now() + 90 * 24 * 60 * 60 * 1000),
              httpOnly: true
            };
            if (process.env.NODE_ENV === 'production') cookieOpt.secure = true;
            res.cookie('jwt', token, cookieOpt).status(statusCode).json({
              status: true,
              token: token,
              user: user
            });

          case 8:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function sendToken(_x2, _x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}(); // Reset User


exports.signUp = catchAsync( /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res, next) {
    var userData, agency, user;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            userData = req.body;
            agency = _.get(req, 'agency._id');
            userData = _.pick(userData, ['name', 'email', 'phoneNumber', 'password', 'passwordConfirm']); //Validate in Mongo

            _context3.t0 = _;
            _context3.next = 6;
            return User.create(_objectSpread(_objectSpread({}, userData), {}, {
              agency: agency
            }));

          case 6:
            _context3.t1 = _context3.sent;
            _context3.t2 = ['password'];
            user = _context3.t0.omit.call(_context3.t0, _context3.t1, _context3.t2);
            sendToken(user, 201, res);

          case 10:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6, _x7) {
    return _ref3.apply(this, arguments);
  };
}());
exports.login = catchAsync( /*#__PURE__*/function () {
  var _ref4 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee4(req, res, next) {
    var _req$body, email, phoneNumber, password, username, u, check;

    return regeneratorRuntime.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _req$body = req.body, email = _req$body.email, phoneNumber = _req$body.phoneNumber, password = _req$body.password;
            username = phoneNumber ? {
              phoneNumber: phoneNumber
            } : {
              email: email
            };
            _context4.next = 4;
            return User.findOne(username).select(['-active', '-createdAt', '+password']);

          case 4:
            u = _context4.sent;

            if (u) {
              _context4.next = 7;
              break;
            }

            return _context4.abrupt("return", next(new AppError('Tài khoản không chính xác', 400)));

          case 7:
            _context4.next = 9;
            return u.checkPassword(u.password, password);

          case 9:
            check = _context4.sent;

            if (check) {
              _context4.next = 12;
              break;
            }

            return _context4.abrupt("return", next(new AppError('Mật khẩu không chính xác', 400)));

          case 12:
            sendToken(u, 200, res);

          case 13:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));

  return function (_x8, _x9, _x10) {
    return _ref4.apply(this, arguments);
  };
}());
exports.forgotPassword = catchAsync( /*#__PURE__*/function () {
  var _ref5 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee5(req, res, next) {
    var user, resetToken;
    return regeneratorRuntime.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            _context5.next = 2;
            return User.findOne({
              phoneNumber: req.body.phoneNumber
            });

          case 2:
            user = _context5.sent;

            if (user) {
              _context5.next = 5;
              break;
            }

            return _context5.abrupt("return", next(new AppError('Email that you enter is not exists', 404)));

          case 5:
            _context5.next = 7;
            return user.createResetPasswordToken();

          case 7:
            resetToken = _context5.sent;
            user.save({
              validateBeforeSave: false
            });
            res.status(200).json({
              status: true,
              message: 'Check your token at email',
              resetToken: resetToken
            });

          case 10:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));

  return function (_x11, _x12, _x13) {
    return _ref5.apply(this, arguments);
  };
}());
exports.sendSMS = catchAsync( /*#__PURE__*/function () {
  var _ref6 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee6(req, res, next) {
    var _req$body2, phoneNumber, recaptchaToken, user, identityToolkit, response, sessionInfo;

    return regeneratorRuntime.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            _req$body2 = req.body, phoneNumber = _req$body2.phoneNumber, recaptchaToken = _req$body2.recaptchaToken;
            _context6.next = 3;
            return User.findOne({
              phoneNumber: phoneNumber
            });

          case 3:
            user = _context6.sent;

            if (user) {
              _context6.next = 6;
              break;
            }

            return _context6.abrupt("return", next(new AppError('Không tìm thấy tài khoản trên hệ thống', 404)));

          case 6:
            identityToolkit = google.identitytoolkit({
              auth: 'AIzaSyAWNdt0TWxlzDw658tEgoE-UJS3ZWJ5PY0',
              version: 'v3'
            });
            _context6.next = 9;
            return identityToolkit.relyingparty.sendVerificationCode({
              phoneNumber: phoneNumber,
              recaptchaToken: recaptchaToken
            });

          case 9:
            response = _context6.sent;
            // save sessionInfo into db. You will need this to verify the SMS code
            sessionInfo = response.data.sessionInfo;
            user.passwordResetToken = sessionInfo;
            user.save();
            res.status(200).json({
              status: true
            });

          case 14:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));

  return function (_x14, _x15, _x16) {
    return _ref6.apply(this, arguments);
  };
}()); // exports.resetByPhone = catchAsync(async (req, res, next) => {
//   const { phoneNumber, verificationCode, newPassword } = req.body;
//   const user = await User.findOne({ phoneNumber });
//   if (!user) {
//     return next(new AppError('Không tìm thấy tài khoản trên hệ thống', 404));
//   }
//   const identityToolkit = google.identitytoolkit({
//     auth: 'AIzaSyAWNdt0TWxlzDw658tEgoE',
//     version: 'v3'
//   });
//   await identityToolkit.relyingparty.verifyPhoneNumber({
//     code: verificationCode,
//     sessionInfo: user.passwordResetToken
//   });
//   user.password = newPassword;
//   user.passwordConfirm = newPassword;
//   await user.save();
//   res.status(200).json({
//     status: true
//   });
// });

exports.resetByPhone = catchAsync( /*#__PURE__*/function () {
  var _ref7 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee7(req, res, next) {
    var _req$body3, newPassword, resetToken, provider, userNumber, user, d;

    return regeneratorRuntime.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            _req$body3 = req.body, newPassword = _req$body3.newPassword, resetToken = _req$body3.resetToken;
            _context7.next = 3;
            return firebaseServices.auth().verifyIdToken(resetToken);

          case 3:
            provider = _context7.sent;

            if (!(provider && provider.phone_number)) {
              _context7.next = 14;
              break;
            }

            userNumber = provider.phone_number.replace(/\+84/, '0');
            _context7.next = 8;
            return User.findOne({
              phoneNumber: userNumber
            });

          case 8:
            user = _context7.sent;
            user.password = newPassword.toString();
            _context7.next = 12;
            return user.save();

          case 12:
            d = _context7.sent;
            return _context7.abrupt("return", res.status(200).json({
              status: true,
              msg: 'Khôi phục mật khẩu thành công!'
            }));

          case 14:
            return _context7.abrupt("return", res.json({
              status: false,
              msg: 'Không tìm thấy thông tin vui lòng thử lại!'
            }));

          case 15:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));

  return function (_x17, _x18, _x19) {
    return _ref7.apply(this, arguments);
  };
}());
exports.resetPassword = catchAsync( /*#__PURE__*/function () {
  var _ref8 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee8(req, res, next) {
    var resetToken, encToken, user, _req$body4, password, passwordConfirm;

    return regeneratorRuntime.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            resetToken = req.params.resetToken;
            encToken = crypto.createHash('sha256').update(resetToken).digest('hex');
            _context8.next = 4;
            return User.findOne({
              passwordResetToken: encToken
            });

          case 4:
            user = _context8.sent;

            if (user) {
              _context8.next = 7;
              break;
            }

            return _context8.abrupt("return", next(new AppError('Token is not found', 404)));

          case 7:
            if (!(user.passwordResetExpired <= Date.now())) {
              _context8.next = 9;
              break;
            }

            return _context8.abrupt("return", next(new AppError('Token is expired', 400)));

          case 9:
            _req$body4 = req.body, password = _req$body4.password, passwordConfirm = _req$body4.passwordConfirm;
            user.password = password;
            user.passwordConfirm = passwordConfirm;
            user.passwordResetToken = undefined;
            user.passwordResetExpired = undefined;
            _context8.next = 16;
            return user.save();

          case 16:
            sendToken(user, 200, res);

          case 17:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));

  return function (_x20, _x21, _x22) {
    return _ref8.apply(this, arguments);
  };
}()); //Middleware

exports.protect = catchAsync( /*#__PURE__*/function () {
  var _ref9 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee9(req, res, next) {
    var token, jwtVerify, verifyJwt, user;
    return regeneratorRuntime.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : undefined;

            if (token) {
              _context9.next = 3;
              break;
            }

            return _context9.abrupt("return", next(new AppError('You have to login to use this function!', 401)));

          case 3:
            jwtVerify = promisify(jwt.verify);
            _context9.next = 6;
            return jwtVerify(token, secret);

          case 6:
            verifyJwt = _context9.sent;
            _context9.next = 9;
            return User.findOne({
              _id: verifyJwt.userId,
              token: token
            });

          case 9:
            user = _context9.sent;

            if (user) {
              _context9.next = 12;
              break;
            }

            return _context9.abrupt("return", next(new AppError('The user is not exists or no longer exists on our system', 401)));

          case 12:
            if (!(user.passwordChangedAt && parseInt(user.passwordChangedAt.getTime() / 1000, 10) > verifyJwt.iat)) {
              _context9.next = 14;
              break;
            }

            return _context9.abrupt("return", next(new AppError('Please login again, the password has been changed', 401)));

          case 14:
            req.user = user;
            next();

          case 16:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));

  return function (_x23, _x24, _x25) {
    return _ref9.apply(this, arguments);
  };
}());

exports.restrictTo = function () {
  for (var _len = arguments.length, roles = new Array(_len), _key = 0; _key < _len; _key++) {
    roles[_key] = arguments[_key];
  }

  return catchAsync( /*#__PURE__*/function () {
    var _ref10 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee10(req, res, next) {
      return regeneratorRuntime.wrap(function _callee10$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              if (_.includes(roles, req.user.role)) {
                _context10.next = 2;
                break;
              }

              return _context10.abrupt("return", next(new AppError("You don't have permission to access this page", 400)));

            case 2:
              next();

            case 3:
            case "end":
              return _context10.stop();
          }
        }
      }, _callee10);
    }));

    return function (_x26, _x27, _x28) {
      return _ref10.apply(this, arguments);
    };
  }());
};

exports.registerDevice = catchAsync( /*#__PURE__*/function () {
  var _ref11 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee11(req, res, next) {
    var _req$body5, deviceID, cdKey, response, agency;

    return regeneratorRuntime.wrap(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            _req$body5 = req.body, deviceID = _req$body5.deviceID, cdKey = _req$body5.cdKey;
            _context11.next = 3;
            return Agency.findOneAndUpdate({
              cdKey: cdKey
            }, {
              deviceID: deviceID
            });

          case 3:
            response = _context11.sent;

            if (!response) {
              _context11.next = 9;
              break;
            }

            _context11.next = 7;
            return Agency.findOne({
              deviceID: deviceID
            });

          case 7:
            agency = _context11.sent;
            return _context11.abrupt("return", res.json({
              success: true,
              tabs: agency.numTabs
            }));

          case 9:
            return _context11.abrupt("return", res.json({
              success: false
            }));

          case 10:
          case "end":
            return _context11.stop();
        }
      }
    }, _callee11);
  }));

  return function (_x29, _x30, _x31) {
    return _ref11.apply(this, arguments);
  };
}());
exports.checkDevice = catchAsync( /*#__PURE__*/function () {
  var _ref12 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee12(req, res, next) {
    var token, deviceID, jwtVerify, verifyJwt, user, agency;
    return regeneratorRuntime.wrap(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            token = req.headers.authorization ? req.headers.authorization.split(' ')[1] : undefined;
            deviceID = req.headers.authdevice; // if (!deviceID && !token)
            //   return next(new AppError('The request not found', 404));

            if (!token) {
              _context12.next = 13;
              break;
            }

            jwtVerify = promisify(jwt.verify);
            _context12.next = 6;
            return jwtVerify(token, secret);

          case 6:
            verifyJwt = _context12.sent;
            _context12.next = 9;
            return User.findById(verifyJwt.userId);

          case 9:
            user = _context12.sent;
            req.user = user;

            if (!(user.role === 'admin')) {
              _context12.next = 13;
              break;
            }

            return _context12.abrupt("return", next());

          case 13:
            if (!deviceID) {
              _context12.next = 18;
              break;
            }

            _context12.next = 16;
            return Agency.findOne({
              deviceID: deviceID
            });

          case 16:
            agency = _context12.sent;

            // if (!agency || !agency.isActive) {
            //   return next(new AppError('Please dont abuse our system, thank you!', 401));
            // }
            if (agency) {
              req.agency = agency;
            }

          case 18:
            return _context12.abrupt("return", next());

          case 19:
          case "end":
            return _context12.stop();
        }
      }
    }, _callee12);
  }));

  return function (_x32, _x33, _x34) {
    return _ref12.apply(this, arguments);
  };
}());