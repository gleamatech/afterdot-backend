import React from "react";
import {CircularProgress} from "@material-ui/core";
import {toAbsoluteUrl} from "../../_helpers";

export function SplashScreen() {
  return (
    <>
      <div className="splash-screen">
        {/* <img
          src={toAbsoluteUrl("/media/logos/logo-mini-md.png")}
          alt="Afterdot logo"
        /> */}
        <span>Afterdot</span>
        <CircularProgress className="splash-screen-spinner" />
      </div>
    </>
  );
}
