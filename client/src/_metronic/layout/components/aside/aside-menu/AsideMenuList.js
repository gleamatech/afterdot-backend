/* eslint-disable jsx-a11y/role-supports-aria-props */
/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from 'react';
import { useLocation } from 'react-router';
import { NavLink } from 'react-router-dom';
import SVG from 'react-inlinesvg';
import { toAbsoluteUrl, checkIsActive } from '../../../../_helpers';

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url)
      ? ` ${!hasSubmenu &&
          'menu-item-active'} menu-item-open menu-item-not-hightlighted`
      : '';
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        {/* Applications */}
        {/* begin::section */}
        <li className="menu-section ">
          {/* Applications */}
          <h4 className="menu-text">Thành phần</h4>
          <i className="menu-icon flaticon-more-v2"></i>
        </li>
        {/* end:: section */}

        {/* Article */}
        {/*begin::1 Level*/}
        <li
          className={`menu-item menu-item-submenu ${getMenuItemActive(
            '/article',
            false
          )}`}
          aria-haspopup="true"
          data-menu-toggle="hover"
        >
          <NavLink className="menu-link menu-toggle" to="/article">
            <span className="svg-icon menu-icon">
              <SVG src={toAbsoluteUrl('/media/svg/icons/Shopping/Bag2.svg')} />
            </span>
            {/* Article */}
            <span className="menu-text">Bài đăng</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}

        {/* Category */}
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive('/category', false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/category">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  '/media/svg/icons/Communication/Add-user.svg'
                )}
              />
            </span>
            {/* Category */}
            <span className="menu-text">Thể loại</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}

        {/* Category */}
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive('/hash-tag', false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/hash-tag">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  '/media/svg/icons/Communication/Add-user.svg'
                )}
              />
            </span>
            <span className="menu-text">Thẻ</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}

        {/* User */}
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive('/user', false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/user">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  '/media/svg/icons/Communication/Add-user.svg'
                )}
              />
            </span>
            {/* User */}
            <span className="menu-text">Quản lý người dùng</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}

        {/* Setting */}
        {/*begin::1 Level*/}
        <li
          className={`menu-item ${getMenuItemActive('/setting', false)}`}
          aria-haspopup="true"
        >
          <NavLink className="menu-link" to="/setting">
            <span className="svg-icon menu-icon">
              <SVG
                src={toAbsoluteUrl(
                  '/media/svg/icons/Communication/Add-user.svg'
                )}
              />
            </span>
            {/* Setting */}
            <span className="menu-text">Cài đặt</span>
          </NavLink>
        </li>
        {/*end::1 Level*/}
      </ul>

      {/* end::Menu Nav */}
    </>
  );
}
