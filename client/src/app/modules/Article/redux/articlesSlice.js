import {createSlice} from "@reduxjs/toolkit";

const initialArtilesState = {
  listLoading: false,
  actionsLoading: false,
  totalCount: 0,
  entities: null,
  lastError: null,
  idArticle: null,
  fileImage: null,
};
export const callTypes = {
  list: "list",
  action: "action"
};

export const articlesSlice = createSlice({
  name: "customers",
  initialState: initialArtilesState,
  reducers: {
    catchError: (state, action) => {
      state.error = `${action.type}: ${action.payload.error}`;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = false;
      } else {
        state.actionsLoading = false;
      }
    },
    startCall: (state, action) => {
      state.error = null;
      if (action.payload.callType === callTypes.list) {
        state.listLoading = true;
      } else {
        state.actionsLoading = true;
      }
    },
    // updateId
    updateIdArticle: (state, action) => {
      state.actionsLoading = false;
      state.idArticle = action.payload;
      state.error = null;
    },
    // update File Image
    updateImage: (state, action) => {
      state.actionsLoading = false;
      state.fileImage = action.payload;
      state.error = null;
    },

  }
});
