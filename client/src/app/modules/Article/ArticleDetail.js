/* eslint-disable jsx-a11y/anchor-is-valid */
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { BaseAPI } from '../../../redux/setupAxios';
import CKEditor from '@ckeditor/ckeditor5-react';
import { Field, Formik } from 'formik';
import React, { useEffect, useMemo, useState, useRef } from 'react';
import { Form } from 'react-bootstrap';
import * as Yup from 'yup';
import {
  Card,
  CardBody,
  CardHeader,
  Input
} from '../../../_metronic/_partials/controls';
import ReactTags from 'react-tag-autocomplete';
import './index.scss';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import Resizer from 'react-image-file-resizer';
import ArticleImageUpload from './ArticleImageUpload';
import { articlesSlice } from './redux/articlesSlice';
import { useDispatch } from 'react-redux';
import { toAbsoluteUrl } from '../../../_metronic/_helpers';
import SVG from 'react-inlinesvg';
import { toast } from 'react-toastify';
const { actions } = articlesSlice;

let timer

export default function ArticleDetailPage() {
  const history = useHistory();
  const articleRedux = useSelector(state => state.articles);
  const dispatch = useDispatch();
  const idArticle = articleRedux.idArticle;
  const fileImage = articleRedux.fileImage;

  const [listTags, setListTags] = useState([]);
  const [suggestionTags, setSuggestionTags] = useState([]);
  const [listCategories, setListCategories] = useState([]);
  const [suggestionCategories, setSuggestionCategories] = useState([]);
  const [contentEditor, setContentEditor] = useState('');
  const [imageLink, setImageLink] = useState();
  const [editor, setEditor] = useState(null);
  const [dataArticle, setdataArticle] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [keyTag, setKeyTag] = useState('')

  const editorRef = useRef()

  const fetchTags = async () => {
    const params = {
      limit: 100000
    };
    try {
      const response = await BaseAPI.get('/api/tags', { params });
      const { data } = response;
      if (!response.status) return;
      const arrTemp = data
        ? data.docs.map((item, index) => ({
            id: item.name,
            name: item.name
          }))
        : [];
      setSuggestionTags(arrTemp);
    } catch (error) {
      console.log(error);
    }
  };
  const fetchCategory = async () => {
    const params = {
      limit: 100000
    };
    try {
      const response = await BaseAPI.get('/api/categories', { params });
      const { data } = response;
      if (!response.status) return;
      const arrTemp = data
        ? data.docs.map((item, index) => ({
            id: item.id,
            name: item.name.vi
          }))
        : [];
      setSuggestionCategories(arrTemp);
    } catch (error) {
      console.log(error);
    }
  };
  const fetchArticle = async () => {
    try {
      const response = await BaseAPI.get(`/api/posts/${idArticle}`);
      const { data } = response;
      if (!response.status) return;
      console.log(data.doc);
      setdataArticle(data.doc);
      const listCategories = [];
      const listTags = [];
      if (
        data &&
        data.doc &&
        data.doc.category &&
        data.doc.tags &&
        data.doc.image
      ) {
        data.doc.tags.map((item, index) => {
          listTags.push({
            name: item,
            id: item
          });
        });
        data.doc.category.map((item, index) => {
          listCategories.push({
            name: item.name.vi,
            id: item.id
          });
        });
        setListTags(listTags);
        setListCategories(listCategories);
        dispatch(
          actions.updateImage([
            {
              data_url: data.doc.image,
              file: ''
            }
          ])
        );
      } else {
        setListTags([]);
        setListCategories([]);
        dispatch(
          actions.updateImage([
            {
              data_url: '',
              file: ''
            }
          ])
        );
      }
    } catch (error) {
      console.log(error);
    }
  };
  const onDeleteTag = i => {
    const deleteTag = listTags.slice(0);
    deleteTag.splice(i, 1);
    setListTags(deleteTag);
  };
  const onDeleteCategory = i => {
    const deleteCategory = listCategories.slice(0);
    deleteCategory.splice(i, 1);
    setListCategories(deleteCategory);
  };

  const onAdditionTag = async (tag) => {
    const filterTag = listTags.filter(item => item.name === tag.name);
    if (filterTag.length) {
      toast.warning("Thẻ không hợp lệ, vui lòng kiểm tra lại");
      return;
    }
    if(!tag.id && tag.name){
      const response = await BaseAPI.post('/api/tags', {
        name: tag.name
      });
      console.log(response)
      if(response.status){
        const { name } = response.data.doc
        const newTag = {
          name,
          id: name
        }
        const addTag = [].concat(listTags, newTag);
        setListTags(addTag);
      }
      setKeyTag('')
      return 
    }
    const addTag = [].concat(listTags, tag);
    setListTags(addTag);
    setKeyTag('')
  };

  const onAdditionCategory = category => {
    const filterCategory = listCategories.filter(
      item => item.name === category.name
    );
    if (filterCategory.length) {
      toast.warning("Thể loại không hợp lệ, vui lòng kiểm tra lại");
      return;
    }
    const addTag = [].concat(listCategories, category);
    setListCategories(addTag);
  };

  const initialValues = {
    title: dataArticle ? dataArticle.title : '',
    image: dataArticle ? dataArticle.image : '',
    author: dataArticle ? dataArticle.author : ''
  };
  const validateSchema = Yup.object().shape({
    title: Yup.string()
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required('Title is required'),
    author: Yup.string()
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required('Title is required')
  });
  const onLoadEditor = (e) => {
    console.log(e)
    setEditor(e);
  }
  const handleChangeImage = async e => {
    setIsLoading(true)
    const file = e.target.files[0];
    console.log(e.target.files)
    e.target.value = null
    const resizeImage = () => {
      return new Promise(async (resolve, reject) => {
        Resizer.imageFileResizer(
          file,
          1300,
          1300,
          'PNG',
          100,
          0,
          uri => {
            resolve(uri);
          },
          'file'
        );
      });
    };
    const fileResized = await resizeImage();
    console.log(fileResized); 
    const formData = new FormData();
    formData.append('image', fileResized);
    try {
      const response = await BaseAPI.post('/api/file', formData);
      if (response && editor) {
        editor.model.change(writer => {
          const paragraph = writer.createElement('paragraph');

          const imageElement = writer.createElement('image', {
            src: response.data,
          })
          editor.model.insertContent(paragraph, writer.createPositionAt(editor.model.document.getRoot(), 'end'));
          editor.model.insertContent(imageElement)
          writer.setSelection(paragraph, 'in');
        })
      }
    } catch (err) {
      console.log(err);
    }
    setIsLoading(false)
  };
  const isImage = useMemo(() => {
    return fileImage;
  }, [fileImage]);
  const handleSubmit = ({ title, author }) => async () => {
    const nameArrTags = [];
    const idArrCategories = [];
    listTags.forEach((item, index) => nameArrTags.push(item.name));
    listCategories.forEach((item, index) => idArrCategories.push(item.id));
    // console.log({
    //   image: fileImage ? fileImage[0].file: "",
    //   title,
    //   content: contentEditor,
    //   tags: nameArrTags,
    //   category: idArrCategories,
    // })
    // return

    if(!title) {
      toast.error("Tiêu đề không được bỏ trống!");
      return
    }

    if(!contentEditor) {
      toast.error("Nội dung không được bỏ trống!");
      return
    }

    if(idArrCategories.length === 0) {
      toast.error("Thể loại không được bỏ trống!");
      return
    }

    if(nameArrTags.length === 0) {
      toast.error("Thẻ không được bỏ trống!");
      return 
    }

    if (!!idArticle) {
      try {
        const response = await BaseAPI.put(`/api/posts/${idArticle}`, {
          title,
          content: contentEditor,
          tags: nameArrTags,
          category: idArrCategories
        });
        if (response.status) {
          toast.success("Cập nhật bài viết thành công");
          history.push('/article');
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      if (isImage && listCategories && listTags) {
        const resizeImage = () => {
          return new Promise(async (resolve, reject) => {
            Resizer.imageFileResizer(
              fileImage[0].file,
              1300,
              1300,
              'PNG',
              100,
              0,
              uri => {
                resolve(uri);
              },
              'file'
            );
          });
        };
        const fileResized = await resizeImage();
        const formData = new FormData();
        formData.append('image', fileResized);
        try {
          const responseImg = await BaseAPI.post('/api/file', formData);
          if (responseImg) {
            try {
              const response = await BaseAPI.post(`/api/posts`, {
                title,
                content: contentEditor,
                tags: nameArrTags,
                category: idArrCategories,
                image: responseImg.data
              });
              if (response.status) {
                toast.success("Tạo bài viết thành công");
                history.push('/article');
              }
            } catch (error) {
              console.log('Create err', error);
            }
          }
        } catch (e) {
          console.log('Err Upload image', e);
        }
      } else {
        toast.error("Vui lòng kiểm tra lại thông tin đã nhập");
      }
    }
  };

  console.log(contentEditor)
  
 
  useEffect(() => {
    fetchCategory();
    fetchTags();

    if (!!idArticle) {
      fetchArticle();
    }
  }, []);
  return (
    <div>
      <Card>
        <CardHeader title="Chi tiết bài đăng" />

        <CardBody>
          <label>Thêm hình ảnh</label>
          <div className="row">
            <div className="col-lg-12 mb-8">
              <ArticleImageUpload />
            </div>
          </div>
          <Formik
            enableReinitialize={true}
            initialValues={initialValues}
            validationSchema={validateSchema}
            onSubmit={handleSubmit}
          >
            {formikProps => {
              const { values, errors } = formikProps;
              const { title, author } = values;
              return (
                <>
                  <Form className="form form-label-right">
                    <div className="form-group row">
                      {/* Title */}
                      <div className="col-lg-12 mb-8">
                        <Field
                          name="title"
                          component={Input}
                          placeholder="Tiêu đề"
                          label="tiêu đề"
                        />
                      </div>
                      <div className="col-lg-12 mb-8">
                        {/* <input
                          type="file"
                          name="myFile"
                          onChange={handleChangeImage}
                        /> */}
                        <span className>
                          <div className="ant-upload ant-upload-select ant-upload-select-text">
                            <span
                              className="ant-upload"
                            >
                              <input
                                className="input-file"
                                type="file"
                                onChange={handleChangeImage}
                                disabled={isLoading}
                              />
                              <button
                                type="button"
                                className="ant-btn"
                              >
                                <span
                                  role="img"
                                  aria-label="upload"
                                  className="anticon anticon-upload"
                                >
                                  <SVG
                                    src={toAbsoluteUrl(
                                      '/media/svg/icons/General/UploadFile.svg'
                                    )}
                                  />
                                </span>
                                <span>{isLoading ? 'Đang tải...' : 'Thêm file'}</span>
                              </button>
                            </span>
                          </div>
                          <div className="ant-upload-list ant-upload-list-text" />
                        </span>
                      </div>
                      <div className="col-lg-12 mb-8 custom-ckeditor">
                        <label>Nội dung</label>
                        <CKEditor
                          ref={editorRef}
                          editor={ClassicEditor}
                          data={
                            dataArticle && dataArticle.content
                              ? dataArticle.content
                              : contentEditor
                          }
                          onChange={(event, editor) => {
                            const data = editor.getData();
                            setContentEditor(data);
                            
                          }}
                          onInit={onLoadEditor}
                          config={{
                            ckfinder: {
                              // uploadUrl: 'https://afterdot.gleamatech.com/public/uploads/'
                              // uploadUrl: 'https://your-organization-id.cke-cs.com/easyimage/upload/'
                              // uploadUrl: 'https://afterdot.gleamatech.com'
                              // uploadUrl: 'https://afterdot.gleamatech.com/api/file'
                              // uploadUrl: 'https://example.com/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images&responseType=json'
                            },
                            toolbar: ["Paragraph","Heading",  "Bold", "Italic", "BlockQuote", "table", "tableToolbar", "CloudServices",   "Image", "ImageCaption", "ImageStyle", "ImageToolbar",  "Link", "List", "MediaEmbed", 
                            "PasteFromOffice",  "TextTransformation" ]
                          }}
                          
                        />
                      </div>
                      <div className="col-lg-12 mb-8">
                        <label>Thể loại</label>
                        <ReactTags
                          tags={listCategories}
                          suggestions={suggestionCategories}
                          onDelete={onDeleteCategory}
                          onAddition={onAdditionCategory}
                          delimiters={['Enter', 'Tab']}
                          placeholderText="Thể loại"
                          minQueryLength="1"
                        />
                      </div>
                      {/* <div className="col-lg-12 mb-8">
                        <Field
                          name="author"
                          component={Input}
                          placeholder="Tác giả"
                          label="tác giả"
                        />
                      </div> */}
                      <div className="col-lg-12 mb-8">
                        <label>Thẻ</label>
                        <ReactTags
                          tags={listTags}
                          suggestions={suggestionTags}
                          onDelete={onDeleteTag}
                          onAddition={onAdditionTag}
                          delimiters={['Enter', 'Tab']}
                          placeholderText="Thẻ"
                          minQueryLength="1"
                          allowNew
                        />
                      </div>
                    </div>
                  </Form>
                  <button type="button" className="btn btn-light btn-elevate">
                    Hủy
                  </button>
                  <button
                    type="submit"
                    onClick={handleSubmit(values)}
                    className="btn btn-primary btn-elevate"
                  >
                    Đồng Ý
                  </button>
                </>
              );
            }}
          </Formik>
        </CardBody>
      </Card>
    </div>
  );
}
