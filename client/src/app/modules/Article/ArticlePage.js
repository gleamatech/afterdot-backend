/* eslint-disable jsx-a11y/anchor-is-valid */
import axios from 'axios';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { Modal } from 'react-bootstrap';
import BootstrapTable from 'react-bootstrap-table-next';
import SVG from 'react-inlinesvg';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify';
import { BaseAPI } from '../../../redux/setupAxios';
import {
  headerSortingClasses,
  sortCaret,
  toAbsoluteUrl
} from '../../../_metronic/_helpers';
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar
} from '../../../_metronic/_partials/controls';
import { articlesSlice } from './redux/articlesSlice';

const { actions } = articlesSlice;
export default function ArticlePage() {
  const [show, setShow] = useState(false);
  const [firstRun, setFirstRun] = useState(true);
  const history = useHistory();
  const [list, setList] = useState([]);
  const [idPost, setIdPost] = useState("");
  const [category, setCategory] = useState([]);
  const [valueSelect, setValueSelect] = useState();
  const dispatch = useDispatch();
 
  const fetchArticle = async () => {
    try {
      const response = await BaseAPI.get('/api/posts');
      if (response.status) {
        const data = response.data.docs;
        setList(data);
      }
    } catch (e) {
      console.log(e);
    }
  };
  const fetchCategory = async () =>{
    const params = {
      limit: 100000
    };
    try {
      const response = await BaseAPI.get('/api/categories',{params});
      if (response.status) {
        const data = response.data.docs;
        setCategory(data);
      }
    } catch (e) {
      console.log(e);
    }
  }
  const handleShowModal = id => () => {
    setShow(true);
    setIdPost(id);
  };
  const handleCancelModal = () => {
    setShow(false);
  };
  const handleSubmitModal = async () => {
    try {
      const response = await BaseAPI.delete(`/api/posts/${idPost}`);
      if (response.status) {
        toast.success("Bài đăng đã được xóa!");
        fetchArticle();
      }
    } catch (error) {
      console.log(error);
    }
    setShow(false);
  };
  const handAddEditAricle = async (id) => {
    dispatch(actions.updateIdArticle(id));
    history.push('/article/detail');
  };
  const handleFilterCategory = async (e) => {
    if(e.target.value==="all"){
      fetchArticle();
    }
    else{
      const arrTempCategory= [];
      arrTempCategory.push(e.target.value)
      try {
        const response = await BaseAPI.post(`/api/posts/category`,{
          category:arrTempCategory,
        });
        if (response.status) {
          response.data&&response.data.docs?setList(response.data.docs):setList([]);
        }
      } catch (error) {
        console.log(error);
      }
    }
  }
  /*Data Table*/
  const [columns, setColumns] = useState([
    {
      dataField: 'title',
      text: 'Tiêu đề',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'author',
      text: 'Tác giả',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'createdAt',
      text: 'Ngày',
      sort: true,
      formatter: (date) => <p>{moment(date).locale('vi').format('LL')}</p>,
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'category',
      text: 'Thể loại',
      sort: false,
      formatter: (category) => !!category.length?(category.map((item,index)=>(index===0?<span>{item.name.vi}</span>:<span>{`, ${item.name.vi}`}</span>))):(<p>Null</p>), 
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'tags',
      text: 'Thẻ',
      sort: false,
      formatter: (tag) => !!tag.length?(tag.map((item,index)=>(index===0?<span>{item}</span>:<span>{`, ${item}`}</span>))):(<p>Null</p>), 
      sortCaret: sortCaret,
      headerSortingClasses,
    },
    {
      dataField: 'id',
      text: 'Thao tác',
      classes: 'text-right pr-0',
      formatter: id => (
        <>
          <a
            title="Edit article"
            className="btn btn-icon btn-light btn-hover-primary btn-sm mx-3"
            onClick={()=>handAddEditAricle(id)}
          >
            <span className="svg-icon svg-icon-md svg-icon-primary">
              <SVG
                src={toAbsoluteUrl('/media/svg/icons/Communication/Write.svg')}
              />
            </span>
          </a>
          <> </>

          <a
            title="Delete article"
            className="btn btn-icon btn-light btn-hover-danger btn-sm"
            onClick={handleShowModal(id)}
          >
            <span className="svg-icon svg-icon-md svg-icon-danger">
              <SVG src={toAbsoluteUrl('/media/svg/icons/General/Trash.svg')} />
            </span>
          </a>
        </>
      ),
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px'
      }
    }
  ]);

  // Modal
  const renderModal = useMemo(() => {
    if (firstRun){
      setFirstRun(!firstRun);
      return;
    }
    return (
      <Modal show={show} aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">
            Delete Aricle
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <span>Are you sure to permanently delete this?</span>
        </Modal.Body>
        <Modal.Footer>
          <div>
            <button
              type="button"
              className="btn btn-light btn-elevate"
              onClick={handleCancelModal}
            >
              Cancel
            </button>
            <> </>
            <button
              type="button"
              onClick={handleSubmitModal}
              className="btn btn-primary btn-elevate"
            >
              OK
            </button>
          </div>
        </Modal.Footer>
      </Modal>
    );
  }, [show]);

  useEffect(() => {
    fetchArticle();
    fetchCategory();

  }, []);
 
  return (
    <div>
      <Card>
        <CardHeader title="Danh sách bài đăng">
          <CardHeaderToolbar>
            <button
              type="button"
              className="btn btn-primary"
              onClick={()=>handAddEditAricle(null)}
            >
              Thêm mới
            </button>
          </CardHeaderToolbar>
        </CardHeader>

        <CardBody>
          <div className="col-lg-3">
            <select
              className="form-control"
              name="status" 
              placeholder="Filter by Status"
              // TODO: Change this code
              onChange={handleFilterCategory}
            >
              <option value="all">Tất cả</option>
              {category.map((item,index)=> <option value={item.id}>{item.name.vi}</option>)}
            </select>
            <small className="form-text text-muted">
              <b>Tìm kiếm</b> theo thể loại
            </small>
          </div>
          <BootstrapTable
            keyField="id"
            columns={columns}
            // data={[...computedList]}
            data={list}
            wrapperClasses="table-responsive"
            classes="table table-head-custom table-vertical-center overflow-hidden"
          />
        </CardBody>
      </Card>
      {renderModal}
    </div>
  );
}
