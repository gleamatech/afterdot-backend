/* eslint-disable jsx-a11y/anchor-is-valid */
import { Modal } from 'react-bootstrap';
import React, { useEffect, useMemo, useState } from 'react';
import ImageUploading from 'react-images-uploading';
import SVG from "react-inlinesvg";
import "./index.scss";
import { toAbsoluteUrl } from '../../../_metronic/_helpers';
import { articlesSlice } from './redux/articlesSlice';
import { useDispatch, useSelector } from 'react-redux';
const { actions } = articlesSlice;
export default function ArticleImageUpload() {
  const articleRedux = useSelector((state) => state.articles);
  const fileImage = articleRedux.fileImage;
  const idArticle = articleRedux.idArticle;
  const [images, setImages] = useState([]);
  const [firstRun, setFirstRun] = useState(false);
  const [show, setShow] = useState(false);
  const [imagePreview, setImagePreview] = useState();
  const maxNumber = 1;
  const dispatch = useDispatch();
  const onChange = (imageList) => {
    dispatch(actions.updateImage(imageList));
    setImages(imageList);
  };
  const onPreview=()=>{
    setShow(!show);
    if(!!idArticle){
        setImagePreview(fileImage[0].data_url);
    }
    else{
        setImagePreview(images[0].data_url);
    }
  }
  const handleCloseModal = ()=>{
    setShow(!show);
  }
    // Modal
    const renderModal = useMemo(() => {
      if (firstRun){
        setFirstRun(!firstRun);
        return;
      }
      return (
        <Modal show={show} aria-labelledby="example-modal-sizes-title-lg">
          <Modal.Header closeButton>
            <Modal.Title id="example-modal-sizes-title-lg">
                <span>Ảnh</span>
                <button type="button" aria-label="Close" className="modal-close" onClick={handleCloseModal}>
                  <span class="modal-close-x">
                    <span role="img" aria-label="close" className="anticon anticon-close modal-close-icon">
                      <svg viewBox="64 64 896 896" focusable="false" data-icon="close" width="1em" height="1em" fill="currentColor" aria-hidden="true">
                        <path d="M563.8 512l262.5-312.9c4.4-5.2.7-13.1-6.1-13.1h-79.8c-4.7 0-9.2 2.1-12.3 5.7L511.6 449.8 295.1 191.7c-3-3.6-7.5-5.7-12.3-5.7H203c-6.8 0-10.5 7.9-6.1 13.1L459.4 512 196.9 824.9A7.95 7.95 0 00203 838h79.8c4.7 0 9.2-2.1 12.3-5.7l216.5-258.1 216.5 258.1c3 3.6 7.5 5.7 12.3 5.7h79.8c6.8 0 10.5-7.9 6.1-13.1L563.8 512z"></path>
                      </svg>
                    </span>
                  </span>
                </button>
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img src={imagePreview?imagePreview:""} alt="" width={450}/>
          </Modal.Body>
          <Modal.Footer>
           
          </Modal.Footer>
        </Modal>
      );
    }, [show,imagePreview]);
    useEffect(() => {
        if(!!idArticle)
            setImages(fileImage);
    }, [fileImage])
  return (
    <div className="App">
    <ImageUploading
    //   multiple
      value={images}
      onChange={onChange}
      maxNumber={maxNumber}
      dataURLKey="data_url"
    >
      {({
        imageList,
        onImageUpload,
        onImageUpdate,
        onImageRemove,
        isDragging,
        dragProps,
      }) => (
        // write your building UI
        <div className="upload__image-wrapper">
          <button 
            className="image-upload image-upload-select image-upload-select-picture-card"
            style={isDragging ? { color: 'red' } : undefined}
            onClick={()=>onImageUpload()}
            {...dragProps}>
            <span tabIndex={0} className="image-upload" role="button">
              <input type="file" accept style={{display: 'none'}} />
              <div>
                <div style={{marginTop: 8}}>Upload</div>
              </div>
            </span>
          </button>
          {imageList.map((image, index) => (
            <div key={index} className="image-item">
              <img src={image['data_url']} alt="" width="100" className="image-upload"/>
              <div className="image-item__btn-wrapper">
                <div onClick={() => onImageUpdate(index)} className="image-icon">
                  <span class="image-icon-span">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/General/Update.svg"
                      )}
                      className="h-50 align-self-center"
                    ></SVG>
                  </span>
                </div>
                <div onClick={() => onImageRemove(index)} className="image-icon">
                  <span span class="image-icon-span">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/General/Trash.svg"
                      )}
                      className="h-50 align-self-center"
                    ></SVG>
                  </span>
                </div>
                <div onClick={() => onPreview(index)} className="image-icon">
                  <span class="image-icon-span">
                    <SVG
                      src={toAbsoluteUrl(
                        "/media/svg/icons/General/Eye.svg"
                      )}
                      className="h-50 align-self-center"
                    ></SVG>
                  </span>
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </ImageUploading>
    {renderModal}
  </div>
  );
}
