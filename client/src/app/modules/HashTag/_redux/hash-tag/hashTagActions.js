import * as requestFromServer from './hashTagCrud';
import { hashTagsSlice, callTypes } from './hashTagSlice';

const { actions } = hashTagsSlice;

export const fetchHashTags = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findHashTags(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.hashTagsFetched({ totalCount, entities }));
    })
    .catch(error => {
      error.clientMessage = "Can't find tag";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchAllHashTags = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .getAllHashTags(queryParams)
    .then(response => {
      const { data, total } = response;
      dispatch(
        actions.hashTagsGetAll({ totalCount: total, entities: data.docs })
      );
    })
    .catch(error => {
      error.clientMessage = "Can't find customers";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchHashTag = id => dispatch => {
  if (!id) {
    return dispatch(actions.hashTagFetched({ hashTagForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getHashTagById(id)
    .then(response => {
      const hashTag = response.data.doc;
      dispatch(actions.hashTagFetched({ hashTagForEdit: hashTag }));
    })
    .catch(error => {
      error.clientMessage = "Can't find hash tag";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteHashTag = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteHashTag(id)
    .then(response => {
      dispatch(actions.hashTagDeleted({ id }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete customer";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const createHashTag = hashTagForCreation => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createHashTag(hashTagForCreation)
    .then(response => {
      if(response.data.errMes) return response.data.errMes;
      dispatch(actions.hashTagCreated({ hashTag: response.data.doc }));
    })
    .catch(error => {
      error.clientMessage = "Can't create customer";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateHashTag = hashTag => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateHashTag(hashTag)
    .then(response => {
      dispatch(actions.hashTagUpdated({ hashTag: response.data.doc }));
    })
    .catch(error => {
      error.clientMessage = "Can't update hashTag";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const updateHashTagsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForHashTags(ids, status)
    .then(() => {
      dispatch(actions.hashTagsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update customers status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteHashTags = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteHashTags(ids)
    .then(response => {
      dispatch(actions.hashTagsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete customers";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
