import { BaseAPI } from '../../../../../redux/setupAxios';

export const HASHTAGS_URL = '/api/tags';

// CREATE =>  POST: add a new tag to the server
export async function createHashTag(hashTag) {
  try {
    const response = await BaseAPI.post(HASHTAGS_URL, hashTag);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log('err', err);
  }
}

// READ
export async function getAllHashTags(queryParams) {
  try {
    const valueInputSearch = queryParams.filter.name
      ? queryParams.filter.name
      : false;
    const pageNumber = queryParams.pageNumber;

    if (valueInputSearch) {
      const response = await BaseAPI.post(
        `${HASHTAGS_URL}/search?page=${pageNumber}`,
        {
          keyword: valueInputSearch
        }
      );
      return response;
    } else {
      const response = await BaseAPI.get(`${HASHTAGS_URL}?page=${pageNumber}`);
      return response;
    }
  } catch (err) {
    console.log(err);
  }
}

export async function getHashTagById(hashTagId) {
  try {
    const response = await BaseAPI.get(`${HASHTAGS_URL}/${hashTagId}`);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findHashTags(queryParams) {
  return BaseAPI.post(`${HASHTAGS_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the tags on the server
export async function updateHashTag(hashTag) {
  try {
    const response = await BaseAPI.put(
      `${HASHTAGS_URL}/${hashTag.id}`,
      hashTag
    );

    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

// UPDATE Status
export function updateStatusForHashTags(ids, status) {
  return BaseAPI.post(`${HASHTAGS_URL}/updateStatusForCustomers`, {
    ids,
    status
  });
}

// DELETE => delete the customer from the server
export async function deleteHashTag(hashTagId) {
  try {
    return await BaseAPI.delete(`${HASHTAGS_URL}/${hashTagId}`);
  } catch (err) {
    console.log(err);
  }
}

// DELETE tags by ids
export function deleteHashTags(ids) {
  return BaseAPI.post(`${HASHTAGS_URL}/deleteHashTags`, { ids });
}
