import hashTagTableMock from './hashTagTableMock';
import MockUtils from './mock.utils';

export default function mockHashTag(mock) {
  mock.onPost('api/hash-tag').reply(({ data }) => {
    const { hashTag } = JSON.parse(data);
    const { name = '' } = hashTag;

    const id = generateUserId();
    const newHashTag = {
      id,
      name
    };
    hashTagTableMock.push(newHashTag);
    return [200, { hashTag: newHashTag }];
  });

  mock.onPost('api/hash-tag/find').reply(config => {
    const mockUtils = new MockUtils();
    const { queryParams } = JSON.parse(config.data);
    const filterCategory = mockUtils.baseFilter(hashTagTableMock, queryParams);
    return [200, filterCategory];
  });

  mock.onPost('api/hash-tag/deleteHashTag').reply(config => {
    const { ids } = JSON.parse(config.data);
    ids.forEach(id => {
      const index = hashTagTableMock.findIndex(el => el.id === id);
      if (index > -1) {
        hashTagTableMock.splice(index, 1);
      }
    });
    return [200];
  });

  mock.onPost('api/hash-tag/updateStatusForHashTag').reply(config => {
    const { ids, status } = JSON.parse(config.data);
    hashTagTableMock.forEach(el => {
      if (ids.findIndex(id => id === el.id) > -1) {
        el.status = status;
      }
    });
    return [200];
  });

  mock.onGet(/api\/hash-tag\/\d+/).reply(config => {
    const id = config.url.match(/api\/hash-tag\/(\d+)/)[1];
    const hashTag = hashTagTableMock.find(el => el.id === +id);
    if (!hashTag) {
      return [400];
    }

    return [200, hashTag];
  });

  mock.onPut(/api\/hash-tag\/\d+/).reply(config => {
    const id = config.url.match(/api\/hash-tag\/(\d+)/)[1];
    const { hashTag } = JSON.parse(config.data);
    const index = hashTagTableMock.findIndex(el => el.id === +id);
    if (!index) {
      return [400];
    }

    hashTagTableMock[index] = { ...hashTag };
    return [200];
  });

  mock.onDelete(/api\/hash-tag\/\d+/).reply(config => {
    const id = config.url.match(/api\/hash-tag\/(\d+)/)[1];
    const index = hashTagTableMock.findIndex(el => el.id === +id);
    hashTagTableMock.splice(index, 1);
    if (!index === -1) {
      return [400];
    }

    return [200];
  });
}

function generateUserId() {
  const ids = hashTagTableMock.map(el => el.id);
  const maxId = Math.max(...ids);
  return maxId + 1;
}
