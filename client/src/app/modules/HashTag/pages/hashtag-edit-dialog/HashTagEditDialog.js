import React, { useEffect, useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as actions from '../../_redux/hash-tag/hashTagActions';
import { HashTagEditDialogHeader } from './HashTagEditDialogHeader';
import { HashTagEditForm } from './HashTagEditForm';
import { useHashTagUIContext } from '../HashTagUIContext';

export function HashTagEditDialog({ id, show, onHide }) {
  // Customers UI Context
  const HashTagUIContext = useHashTagUIContext();
  const HashTagUIProps = useMemo(() => {
    return {
      initHashTag: HashTagUIContext.initHashTag
    };
  }, [HashTagUIContext]);

  // Customers Redux state
  const dispatch = useDispatch();
  const { actionsLoading, hashTagForEdit } = useSelector(
    state => ({
      actionsLoading: state.hashTags.actionsLoading,
      hashTagForEdit: state.hashTags.hashTagForEdit
    }),
    shallowEqual
  );
  useEffect(() => {
    // server call for getting HashTag by id
    dispatch(actions.fetchHashTag(id));
  }, [id, dispatch]);

  // server request for saving HashTag
  const saveHashTag = hashTag => {
    if (!id) {
      // server request for creating HashTag
      dispatch(actions.createHashTag(hashTag)).then(() => onHide());
    } else {
      // server request for updating HashTag
      dispatch(actions.updateHashTag(hashTag)).then(() => onHide());
    }
  };
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <HashTagEditDialogHeader id={id} />
      <HashTagEditForm
        saveHashTag={saveHashTag}
        actionsLoading={actionsLoading}
        hashTag={hashTagForEdit || HashTagUIProps.initHashTag}
        onHide={onHide}
      />
    </Modal>
  );
}
