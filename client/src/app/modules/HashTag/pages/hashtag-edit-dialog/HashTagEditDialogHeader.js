import React, { useState, useEffect } from 'react';
import { shallowEqual, useSelector } from 'react-redux';
import { Modal } from 'react-bootstrap';
import { ModalProgressBar } from '../../../../../_metronic/_partials/controls';

export function HashTagEditDialogHeader({ id }) {
  // Customers Redux state
  const { hashTagForEdit, actionsLoading } = useSelector(
    state => ({
      hashTagForEdit: state.hashTags.hashTagForEdit,
      actionsLoading: state.hashTags.actionsLoading
    }),
    shallowEqual
  );

  const [title, setTitle] = useState('');
  // Title couting
  useEffect(() => {
    let _title = id ? '' : 'Thêm mới thẻ';
    if (hashTagForEdit && id) {
      _title = `Chỉnh sửa thẻ'${hashTagForEdit.name}'`;
    }

    setTitle(_title);
    // eslint-disable-next-line
  }, [hashTagForEdit, actionsLoading]);

  return (
    <>
      {actionsLoading && <ModalProgressBar />}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">{title}</Modal.Title>
      </Modal.Header>
    </>
  );
}
