// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, {
  PaginationProvider
} from 'react-bootstrap-table2-paginator';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as actions from '../../_redux/hash-tag/hashTagActions';
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
  headerSortingClasses
} from '../../../../../_metronic/_helpers';
import * as uiHelpers from '../HashTagUIHelpers';
import * as columnFormatters from './column-formatters';
import { Pagination } from '../../../../../_metronic/_partials/controls';
import { useHashTagUIContext } from '../HashTagUIContext';

export function HashTagsTable() {
  // Customers UI Context
  const hashTagUIContext = useHashTagUIContext();
  const hashTagUIProps = useMemo(() => {
    return {
      ids: hashTagUIContext.ids,
      setIds: hashTagUIContext.setIds,
      queryParams: hashTagUIContext.queryParams,
      setQueryParams: hashTagUIContext.setQueryParams,
      openEditHashTagDialog: hashTagUIContext.openEditHashTagDialog,
      openDeleteHashTagDialog: hashTagUIContext.openDeleteHashTagDialog
    };
  }, [hashTagUIContext]);

  // Getting current state of category list from store (Redux)
  const { currentState } = useSelector(
    state => ({ currentState: state.hashTags }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // HashTags Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    hashTagUIProps.setIds([]);
    // server call by queryParams
    // dispatch(actions.fetchHashTags(hashTagUIProps.queryParams));

    // server call all tags by queryParams
    dispatch(actions.fetchAllHashTags(hashTagUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [hashTagUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: 'name',
      text: 'Tên',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'action',
      text: 'Thao tác',
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditHashTagDialog: hashTagUIProps.openEditHashTagDialog,
        openDeleteHashTagDialog: hashTagUIProps.openDeleteHashTagDialog
      },
      classes: 'text-right pr-0',
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px'
      }
    }
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: hashTagUIProps.queryParams.pageSize,
    page: hashTagUIProps.queryParams.pageNumber
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                bordered={false}
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  hashTagUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: hashTagUIProps.ids,
                  setIds: hashTagUIProps.setIds
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
