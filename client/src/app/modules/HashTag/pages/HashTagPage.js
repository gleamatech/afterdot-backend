import React from 'react';
import { Route } from 'react-router-dom';
import { HashTagsLoadingDialog } from './hashtags-loading-dialog/HashTagsLoadingDialog';
import { HashTagEditDialog } from './hashtag-edit-dialog/HashTagEditDialog';
import { HashTagDeleteDialog } from './hashtag-delete-dialog/HashTagDeleteDialog';
import { HashTagUIProvider } from './HashTagUIContext';
import { HashTagCard } from './HashTagCard';

export default function HashTabPage({ history }) {
  const hashTagUIEvents = {
    newHashTagButtonClick: () => {
      history.push('/hash-tag/new');
    },
    openEditHashTagDialog: id => {
      history.push(`/hash-tag/${id}/edit`);
    },
    openDeleteHashTagDialog: id => {
      history.push(`/hash-tag/${id}/delete`);
    }
  };

  return (
    <HashTagUIProvider hashTagUIEvents={hashTagUIEvents}>
      <HashTagsLoadingDialog />
      <Route path="/hash-tag/new">
        {({ history, match }) => (
          <HashTagEditDialog
            show={match != null}
            onHide={() => {
              history.push('/hash-tag');
            }}
          />
        )}
      </Route>
      <Route path="/hash-tag/:id/edit">
        {({ history, match }) => (
          <HashTagEditDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push('/hash-tag');
            }}
          />
        )}
      </Route>
      <Route path="/hash-tag/:id/delete">
        {({ history, match }) => (
          <HashTagDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push('/hash-tag');
            }}
          />
        )}
      </Route>
      <HashTagCard />
    </HashTagUIProvider>
  );
}
