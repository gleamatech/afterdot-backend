import React, { useMemo } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar
} from '../../../../_metronic/_partials/controls';
import { HashTagsTable } from './hashtags-table/HashTagsTable';
import { HashTagsFilter } from './hashtags-filter/HashTagsFilter';
import { useHashTagUIContext } from './HashTagUIContext';

export function HashTagCard() {
  const HashTagUIContext = useHashTagUIContext();
  const HashTagUIProps = useMemo(() => {
    return {
      ids: HashTagUIContext.ids,
      newHashTagButtonClick: HashTagUIContext.newHashTagButtonClick
    };
  }, [HashTagUIContext]);

  return (
    <Card>
      <CardHeader title="Thẻ">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={HashTagUIProps.newHashTagButtonClick}
          >
            Thêm mới
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <HashTagsFilter />
        <HashTagsTable />
      </CardBody>
    </Card>
  );
}
