import React, { useMemo } from 'react';
import { Formik } from 'formik';
import { isEqual } from 'lodash';
import { useHashTagUIContext } from '../HashTagUIContext';

let timer;

const prepareFilter = (queryParams, values) => {
  const { searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};
  // Filter by status
  if (searchText) {
    filter.name = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function HashTagsFilter({ listLoading }) {
  // Category UI Context
  const hashTagUIContext = useHashTagUIContext();
  const hashTagUIProps = useMemo(() => {
    return {
      queryParams: hashTagUIContext.queryParams,
      setQueryParams: hashTagUIContext.setQueryParams
    };
  }, [hashTagUIContext]);

  // queryParams, setQueryParams,
  const applyFilter = values => {
    const newQueryParams = prepareFilter(hashTagUIProps.queryParams, values);
    if (!isEqual(newQueryParams, hashTagUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      // update list by queryParams
      hashTagUIProps.setQueryParams(newQueryParams);
    }
  };

  const handleChangeKeyword = (e, saveState, callback) => {
    clearTimeout(timer)
    saveState && saveState('searchText', e.target.value)
    timer = setTimeout(() => {
      callback && callback()
    },1000)
  }

  return (
    <>
      <Formik
        initialValues={{
          searchText: ''
        }}
        onSubmit={values => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue
        }) => (
          <form onSubmit={handleSubmit} className="form form-label-right">
            <div className="form-group row">
              <div className="col-lg-2">
                <input
                  type="text"
                  className="form-control"
                  name="searchText"
                  placeholder="Tìm kiếm"
                  onBlur={handleBlur}
                  value={values.searchText}
                  onChange={e => {
                    // setFieldValue('searchText', e.target.value);
                    // handleSubmit();
                    handleChangeKeyword(e, setFieldValue, handleSubmit);
                  }}
                  
                />
                <small className="form-text text-muted">
                  <b>Tìm kiếm</b> tất cả.
                </small>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
}
