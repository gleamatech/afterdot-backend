import React, { createContext, useContext, useState, useCallback } from 'react';
import { isEqual, isFunction } from 'lodash';
import { initialFilter } from './HashTagUIHelpers';

const HashTagUIContext = createContext();

export function useHashTagUIContext() {
  return useContext(HashTagUIContext);
}

export const HashTagUIConsumer = HashTagUIContext.Consumer;

export function HashTagUIProvider({ hashTagUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback(nextQueryParams => {
    setQueryParamsBase(prevQueryParams => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const initHashTag = {
    id: undefined,
    name: ''
  };

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    initHashTag,
    newHashTagButtonClick: hashTagUIEvents.newHashTagButtonClick,
    openEditHashTagDialog: hashTagUIEvents.openEditHashTagDialog,
    openDeleteHashTagDialog: hashTagUIEvents.openDeleteHashTagDialog
  };

  return (
    <HashTagUIContext.Provider value={value}>
      {children}
    </HashTagUIContext.Provider>
  );
}
