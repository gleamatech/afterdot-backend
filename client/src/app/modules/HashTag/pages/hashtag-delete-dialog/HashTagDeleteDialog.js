import React, { useEffect, useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import { ModalProgressBar } from '../../../../../_metronic/_partials/controls';
import * as actions from '../../_redux/hash-tag/hashTagActions';
import { useHashTagUIContext } from '../HashTagUIContext';

export function HashTagDeleteDialog({ id, show, onHide }) {
  // HashTags UI Context
  const HashTagsUIContext = useHashTagUIContext();
  const HashTagsUIProps = useMemo(() => {
    return {
      setIds: HashTagsUIContext.setIds,
      queryParams: HashTagsUIContext.queryParams
    };
  }, [HashTagsUIContext]);

  // HashTags Redux state
  const dispatch = useDispatch();
  const { isLoading } = useSelector(
    state => ({ isLoading: state.hashTags.actionsLoading }),
    shallowEqual
  );

  // if !id we should close modal
  useEffect(() => {
    if (!id) {
      onHide();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id]);

  // looking for loading/dispatch
  useEffect(() => {}, [isLoading, dispatch]);

  const deleteHashTag = () => {
    // server request for deleting tag by id
    dispatch(actions.deleteHashTag(id)).then(() => {
      // refresh list after deletion
      // dispatch(actions.fetchHashTags(HashTagsUIProps.queryParams));
      // clear selections list
      HashTagsUIProps.setIds([]);
      // closing delete modal
      onHide();
    });
  };

  return (
    <Modal
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      {/*begin::Loading*/}
      {isLoading && <ModalProgressBar />}
      {/*end::Loading*/}
      <Modal.Header closeButton>
        <Modal.Title id="example-modal-sizes-title-lg">
          Customer Delete
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {!isLoading && (
          <span>Are you sure to permanently delete this customer?</span>
        )}
        {isLoading && <span>Customer is deleting...</span>}
      </Modal.Body>
      <Modal.Footer>
        <div>
          <button
            type="button"
            onClick={onHide}
            className="btn btn-light btn-elevate"
          >
            Cancel
          </button>
          <> </>
          <button
            type="button"
            onClick={deleteHashTag}
            className="btn btn-primary btn-elevate"
          >
            Delete
          </button>
        </div>
      </Modal.Footer>
    </Modal>
  );
}
