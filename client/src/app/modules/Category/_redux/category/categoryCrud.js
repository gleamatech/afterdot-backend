import { BaseAPI } from '../../../../../redux/setupAxios';
export const CATEGORIES_URL = 'api/categories';
// CREATE =>  POST: add a new category to the server
export async function createCategory(category) {
  const { vi, en } = category;
  const params = {
    name: {
      vi,
      en
    }
  };
  try {
    const response = await BaseAPI.post(CATEGORIES_URL, params);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log('err', err);
  }
}

// READ
export async function getAllCategories(queryParams) {
  try {
    const valueInputSearch = queryParams.filter.name
      ? queryParams.filter.name
      : false;
    const pageNumber = queryParams.pageNumber;

    if (valueInputSearch) {
      const response = await BaseAPI.post(
        `${CATEGORIES_URL}/search?page=${pageNumber}`,
        {
          keyword: valueInputSearch,
          lang: 'en'
        }
      );
      return response;
    } else {
      const response = await BaseAPI.get(
        `${CATEGORIES_URL}?page=${pageNumber}`
      );
      return response;
    }
  } catch (err) {
    console.log(err);
  }
}

export async function getCategoryById(categoryId) {
  try {
    const response = await BaseAPI.get(`${CATEGORIES_URL}/${categoryId}`);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findCategories(queryParams) {
  return BaseAPI.post(`${CATEGORIES_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the customer on the server
export async function updateCategory(category) {
  const { vi, en } = category;
  const params = {
    name: {
      vi,
      en
    }
  };
  try {
    const response = await BaseAPI.put(
      `${CATEGORIES_URL}/${category.id}`,
      params
    );
    console.log('response', response);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log('err', err);
  }
}

// UPDATE Status
export function updateStatusForCategories(ids, status) {
  return BaseAPI.post(`${CATEGORIES_URL}/updateStatusForCustomers`, {
    ids,
    status
  });
}

// DELETE => delete the customer from the server
export function deleteCategory(categoryId) {
  return BaseAPI.delete(`${CATEGORIES_URL}/${categoryId}`);
}

// DELETE Customers by ids
export function deleteCategories(ids) {
  return BaseAPI.post(`${CATEGORIES_URL}/deleteCustomers`, { ids });
}
