import React, { createContext, useContext, useState, useCallback } from 'react';
import { isEqual, isFunction } from 'lodash';
import { initialFilter } from './CategoryUIHelpers';

const CategoryUIContext = createContext();

export function useCategoryUIContext() {
  return useContext(CategoryUIContext);
}

export const CategoryUIConsumer = CategoryUIContext.Consumer;

export function CategoryUIProvider({ categoryUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback(nextQueryParams => {
    setQueryParamsBase(prevQueryParams => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const initCategory = { id: undefined, vi: '', en: '' };

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    initCategory,
    newCategoryButtonClick: categoryUIEvents.newCategoryButtonClick,
    openEditCategoryDialog: categoryUIEvents.openEditCategoryDialog,
    openDeleteCategoryDialog: categoryUIEvents.openDeleteCategoryDialog
  };

  return (
    <CategoryUIContext.Provider value={value}>
      {children}
    </CategoryUIContext.Provider>
  );
}
