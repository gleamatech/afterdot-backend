// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, {
  PaginationProvider
} from 'react-bootstrap-table2-paginator';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as actions from '../../_redux/category/categoryActions';
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
  headerSortingClasses
} from '../../../../../_metronic/_helpers';
import * as uiHelpers from '../CategoryUIHelpers';
import * as columnFormatters from './column-formatters';
import { Pagination } from '../../../../../_metronic/_partials/controls';
import { useCategoryUIContext } from '../CategoryUIContext';

export function CategoriesTable() {
  // Customers UI Context
  const categoryUIContext = useCategoryUIContext();
  const categoryUIProps = useMemo(() => {
    return {
      ids: categoryUIContext.ids,
      setIds: categoryUIContext.setIds,
      queryParams: categoryUIContext.queryParams,
      setQueryParams: categoryUIContext.setQueryParams,
      openEditCategoryDialog: categoryUIContext.openEditCategoryDialog,
      openDeleteCategoryDialog: categoryUIContext.openDeleteCategoryDialog
    };
  }, [categoryUIContext]);

  // Getting current state of category list from store (Redux)
  const { currentState } = useSelector(
    state => ({ currentState: state.categories }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Customers Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    categoryUIProps.setIds([]);
    // server call by queryParams
    // dispatch(actions.fetchCategories(categoryUIProps.queryParams));

    dispatch(actions.fetchAllCategories(categoryUIProps.queryParams));

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [categoryUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: 'name.en',
      text: 'Tên việt hóa',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'name.vi',
      text: 'Tên',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'action',
      text: 'Thao tác',
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditCategoryDialog: categoryUIProps.openEditCategoryDialog,
        openDeleteCategoryDialog: categoryUIProps.openDeleteCategoryDialog
      },
      classes: 'text-right pr-0',
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px'
      }
    }
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: categoryUIProps.queryParams.pageSize,
    page: categoryUIProps.queryParams.pageNumber
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                bordered={false}
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  categoryUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: categoryUIProps.ids,
                  setIds: categoryUIProps.setIds
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
