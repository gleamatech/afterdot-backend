import React, { useMemo } from 'react';
import { Formik } from 'formik';
import { isEqual } from 'lodash';
import { useCategoryUIContext } from '../CategoryUIContext';

let timer

const prepareFilter = (queryParams, values) => {
  const { searchText } = values;
  const newQueryParams = { ...queryParams };
  const filter = {};

  if (searchText) {
    filter.name = searchText;
  }
  newQueryParams.filter = filter;
  return newQueryParams;
};

export function CategoriesFilter({ listLoading }) {
  // Category UI Context
  const categoryUIContext = useCategoryUIContext();
  const categoryUIProps = useMemo(() => {
    return {
      queryParams: categoryUIContext.queryParams,
      setQueryParams: categoryUIContext.setQueryParams
    };
  }, [categoryUIContext]);

  // queryParams, setQueryParams,
  const applyFilter = values => {
    const newQueryParams = prepareFilter(categoryUIProps.queryParams, values);
    if (!isEqual(newQueryParams, categoryUIProps.queryParams)) {
      newQueryParams.pageNumber = 1;
      // update list by queryParams
      categoryUIProps.setQueryParams(newQueryParams);
    }
  };

  const handleChangeKeyword = (e, saveState, callback) => {
    clearTimeout(timer)
    saveState && saveState('searchText', e.target.value)
    timer = setTimeout(() => {
      callback && callback()
    },1000)
  }

  return (
    <>
      <Formik
        initialValues={{
          searchText: ''
        }}
        onSubmit={values => {
          applyFilter(values);
        }}
      >
        {({
          values,
          handleSubmit,
          handleBlur,
          handleChange,
          setFieldValue
        }) => (
          <form onSubmit={handleSubmit} className="form form-label-right">
            <div className="form-group row">
              <div className="col-lg-2">
                <input
                  type="text"
                  className="form-control"
                  name="searchText"
                  placeholder="Tìm kiếm"
                  onBlur={handleBlur}
                  value={values.searchText}
                  onChange={e => {
                    // setFieldValue('searchText', e.target.value);
                    // handleSubmit();
                    handleChangeKeyword(e, setFieldValue, handleSubmit)
                  }}
                />
                <small className="form-text text-muted">
                  <b>Tìm kiếm</b> tất cả.
                </small>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </>
  );
}
