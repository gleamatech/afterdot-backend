// Form is based on Formik
// Data validation is based on Yup
// Please, be familiar with article first:
// https://hackernoon.com/react-form-validation-with-formik-and-yup-8b76bda62e10
import React from 'react';
import { Modal } from 'react-bootstrap';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { Input } from '../../../../../_metronic/_partials/controls';

// Validation schema
const CategoryEditSchema = Yup.object().shape({
  vi: Yup.string()
    .min(3, 'Minimum 3 symbols')
    .max(50, 'Maximum 50 symbols')
    .required('Category is required'),
  en: Yup.string()
    .min(3, 'Tối thiểu phải có 3 ký tự')
    .max(50, 'Tối đa phải có 50 ký tự')
    .required('Vui lòng nhập mục lục')
});

export function CategoryEditForm({
  saveCategory,
  category,
  actionsLoading,
  onHide
}) {
  return (
    <>
      <Formik
        enableReinitialize={true}
        initialValues={category}
        validationSchema={CategoryEditSchema}
        onSubmit={values => {
          saveCategory(values);
        }}
      >
        {({ handleSubmit }) => (
          <>
            <Modal.Body className="overlay overlay-block cursor-default">
              {actionsLoading && (
                <div className="overlay-layer bg-transparent">
                  <div className="spinner spinner-lg spinner-success" />
                </div>
              )}
              <Form className="form form-label-right">
                <div className="form-group row">
                  {/* Start vietnamese*/}
                  <div className="col-lg-4">
                    <Field
                      name="vi"
                      component={Input}
                      placeholder="Tên Việt Hoá"
                      label="Tên Việt Hoá"
                    />
                  </div>
                  {/* End vietnamese*/}
                  {/* Start english*/}
                  <div className="col-lg-4">
                    <Field
                      name="en"
                      component={Input}
                      placeholder="Tên"
                      label="Tên"
                      // customFeedbackLabel="Vui lòng nhập tên mục lục"
                    />
                  </div>
                  {/* End english*/}
                </div>
              </Form>
            </Modal.Body>
            <Modal.Footer>
              <button
                type="button"
                onClick={onHide}
                className="btn btn-light btn-elevate"
              >
                Hủy
              </button>
              <> </>
              <button
                type="submit"
                onClick={() => handleSubmit()}
                className="btn btn-primary btn-elevate"
              >
                Đồng ý
              </button>
            </Modal.Footer>
          </>
        )}
      </Formik>
    </>
  );
}
