import React, { useMemo } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar
} from '../../../../_metronic/_partials/controls';
import { CategoriesFilter } from './categories-filter/CategoriesFilter';
import { CategoriesTable } from './categories-table/CategoriesTable';
// import { CustomersGrouping } from './customers-grouping/CustomersGrouping';
import { useCategoryUIContext } from './CategoryUIContext';

export function CategoryCard() {
  const CategoryUIContext = useCategoryUIContext();
  const CategoryUIProps = useMemo(() => {
    return {
      ids: CategoryUIContext.ids,
      newCategoryButtonClick: CategoryUIContext.newCategoryButtonClick
    };
  }, [CategoryUIContext]);

  return (
    <Card>
      <CardHeader title="Danh sách thể loại">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={CategoryUIProps.newCategoryButtonClick}
          >
            Thêm mới
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <CategoriesFilter />
        <CategoriesTable />
      </CardBody>
    </Card>
  );
}
