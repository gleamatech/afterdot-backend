import React from 'react';
import { Route } from 'react-router-dom';
import { CategoriesLoadingDialog } from './categories-loading-dialog/CategoriesLoadingDialog';
import { CategoryEditDialog } from './category-edit-dialog/CategoryEditDialog';
import { CategoryDeleteDialog } from './category-delete-dialog/CategoryDeleteDialog';
import { CategoryUIProvider } from './CategoryUIContext';
import { CategoryCard } from './CategoryCard';

export default function CategoryPage({ history }) {
  const categoryUIEvents = {
    newCategoryButtonClick: () => {
      history.push('/category/new');
    },
    openEditCategoryDialog: id => {
      history.push(`/category/${id}/edit`);
    },
    openDeleteCategoryDialog: id => {
      history.push(`/category/${id}/delete`);
    }
  };

  return (
    <CategoryUIProvider categoryUIEvents={categoryUIEvents}>
      <CategoriesLoadingDialog />
      <Route path="/category/new">
        {({ history, match }) => (
          <CategoryEditDialog
            show={match != null}
            onHide={() => {
              history.push('/category');
            }}
          />
        )}
      </Route>
      <Route path="/category/:id/edit">
        {({ history, match }) => (
          <CategoryEditDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push('/category');
            }}
          />
        )}
      </Route>
      <Route path="/category/:id/delete">
        {({ history, match }) => (
          <CategoryDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push('/category');
            }}
          />
        )}
      </Route>

      <CategoryCard />
    </CategoryUIProvider>
  );
}
