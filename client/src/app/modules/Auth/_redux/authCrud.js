import { BaseAPI } from '../../../../redux/setupAxios';
export const LOGIN_URL = '/api/users/login';
export const REGISTER_URL = '/api/users/register';
export const REQUEST_PASSWORD_URL = '/api/users/forgotpassword';

export const ME_URL = '/api/users';

export async function login(email, password) {
  try {
    const response = await BaseAPI.post(LOGIN_URL, { email, password });
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

export async function register(
  email,
  fullname,
  phoneNumber,
  username,
  password
) {
  try {
    const params = {
      name: username,
      email,
      password,
      passwordConfirm: password,
      phoneNumber
    };

    const response = await BaseAPI.post(REGISTER_URL, params);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

export async function requestPassword(phoneNumber) {
  try {
    const response = await BaseAPI.post(REQUEST_PASSWORD_URL, { phoneNumber });
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

export function getUserByToken() {
  // Authorization head should be fulfilled in interceptor.
  return BaseAPI.get(ME_URL);
}

export async function getUserById(id) {
  try {
    const response = await BaseAPI.get(`${ME_URL}/${id}`);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}
