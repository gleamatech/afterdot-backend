import React, { createContext, useContext, useState, useCallback } from 'react';
import { isEqual, isFunction } from 'lodash';
import { initialFilter } from './userUIHelpers';

const UserUIContext = createContext();

export function useUserUIContext() {
  return useContext(UserUIContext);
}

export const UserUIConsumer = UserUIContext.Consumer;

export function UserUIProvider({ userUIEvents, children }) {
  const [queryParams, setQueryParamsBase] = useState(initialFilter);
  const [ids, setIds] = useState([]);
  const setQueryParams = useCallback(nextQueryParams => {
    setQueryParamsBase(prevQueryParams => {
      if (isFunction(nextQueryParams)) {
        nextQueryParams = nextQueryParams(prevQueryParams);
      }

      if (isEqual(prevQueryParams, nextQueryParams)) {
        return prevQueryParams;
      }

      return nextQueryParams;
    });
  }, []);

  const initHashTag = {
    name: ''
  };

  const value = {
    queryParams,
    setQueryParamsBase,
    ids,
    setIds,
    setQueryParams,
    initHashTag,
    newUserButtonClick: userUIEvents.newUserButtonClick,
    openEditUserDialog: userUIEvents.openEditUserDialog,
    openDeleteUserDialog: userUIEvents.openDeleteUserDialog
  };

  return (
    <UserUIContext.Provider value={value}>{children}</UserUIContext.Provider>
  );
}
