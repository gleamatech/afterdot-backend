import React from 'react';
import { Route } from 'react-router-dom';
import { UserLoadingDialog } from './users-loading-dialog/UserLoadingDialog';
import { UserEditDialog } from './user-edit-dialog/UserEditDialog';
import { UserDeleteDialog } from './user-delete-dialog/UserDeleteDialog';
import { UserUIProvider } from './userUIContext';
import { UserCard } from './userCard';
import { useDispatch } from 'react-redux';
import * as actions from '../_redux/users/userActions';
import userDetailPage from './userDetailPage';

export default function UserPage({ history }) {
  const dispatch = useDispatch();
  const userUIEvents = {
    newUserButtonClick: () => {
      history.push('/user-ce/detail');
    },
    openEditUserDialog: id => {
      // Save user getById to store and call it's a userDetailPage
      dispatch(actions.fetchUser(id)).then(() =>
        history.push('/user-ce/detail')
      );
    },
    openDeleteUserDialog: id => {
      history.push(`/user/${id}/delete`);
    }
  };

  return (
    <UserUIProvider userUIEvents={userUIEvents}>
      <UserLoadingDialog />
      <Route path="/user/:id/delete">
        {({ history, match }) => (
          <UserDeleteDialog
            show={match != null}
            id={match && match.params.id}
            onHide={() => {
              history.push('/user');
            }}
          />
        )}
      </Route>
      <UserCard />
    </UserUIProvider>
  );
}
