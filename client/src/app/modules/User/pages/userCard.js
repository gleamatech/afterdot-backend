import React, { useMemo } from 'react';
import {
  Card,
  CardBody,
  CardHeader,
  CardHeaderToolbar
} from '../../../../_metronic/_partials/controls';
import { UsersTable } from './users-table/UsersTable';
import { UsersFilter } from './users-filter/UsersFilter';
import { useUserUIContext } from './userUIContext';

export function UserCard() {
  const UserUIContext = useUserUIContext();
  const UserUIProps = useMemo(() => {
    return {
      ids: UserUIContext.ids,
      newUserButtonClick: UserUIContext.newUserButtonClick
    };
  }, [UserUIContext]);

  return (
    <Card>
      <CardHeader title="Danh sách người dùng">
        <CardHeaderToolbar>
          <button
            type="button"
            className="btn btn-primary"
            onClick={UserUIProps.newUserButtonClick}
          >
            Thêm mới
          </button>
        </CardHeaderToolbar>
      </CardHeader>
      <CardBody>
        <UsersFilter />
        <UsersTable />
      </CardBody>
    </Card>
  );
}
