// React bootstrap table next =>
// DOCS: https://react-bootstrap-table.github.io/react-bootstrap-table2/docs/
// STORYBOOK: https://react-bootstrap-table.github.io/react-bootstrap-table2/storybook/index.html
import React, { useEffect, useMemo } from 'react';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory, {
  PaginationProvider
} from 'react-bootstrap-table2-paginator';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as actions from '../../_redux/users/userActions';
import {
  getSelectRow,
  getHandlerTableChange,
  NoRecordsFoundMessage,
  PleaseWaitMessage,
  sortCaret,
  headerSortingClasses
} from '../../../../../_metronic/_helpers';
import * as uiHelpers from '../userUIHelpers';
import * as columnFormatters from './column-formatters';
import { Pagination } from '../../../../../_metronic/_partials/controls';
import { useUserUIContext } from '../userUIContext';

export function UsersTable() {
  // Users UI Context
  const userUIContext = useUserUIContext();
  const userUIProps = useMemo(() => {
    return {
      ids: userUIContext.ids,
      setIds: userUIContext.setIds,
      queryParams: userUIContext.queryParams,
      setQueryParams: userUIContext.setQueryParams,
      openEditUserDialog: userUIContext.openEditUserDialog,
      openDeleteUserDialog: userUIContext.openDeleteUserDialog
    };
  }, [userUIContext]);

  // Getting current state of category list from store (Redux)
  const { currentState } = useSelector(
    state => ({ currentState: state.users }),
    shallowEqual
  );
  const { totalCount, entities, listLoading } = currentState;
  // Users Redux state
  const dispatch = useDispatch();
  useEffect(() => {
    // clear selections list
    userUIProps.setIds([]);
    // server call by queryParams
    // dispatch(actions.fetchUsers(userUIProps.queryParams));

    // // server call all tags by queryParams
    dispatch(actions.fetchAllUsers(userUIProps.queryParams));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userUIProps.queryParams, dispatch]);
  // Table columns
  const columns = [
    {
      dataField: 'name',
      text: 'Tên',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'email',
      text: 'Email',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    // {
    //   dataField: 'phoneNumber',
    //   text: 'Số điện thoại',
    //   sort: true,
    //   sortCaret: sortCaret,
    //   headerSortingClasses
    // },
    {
      dataField: 'role',
      text: 'Quyền',
      sort: true,
      sortCaret: sortCaret,
      headerSortingClasses
    },
    {
      dataField: 'action',
      text: 'Thao tác',
      formatter: columnFormatters.ActionsColumnFormatter,
      formatExtraData: {
        openEditUserDialog: userUIProps.openEditUserDialog,
        openDeleteUserDialog: userUIProps.openDeleteUserDialog
      },
      classes: 'text-right pr-0',
      headerClasses: 'text-right pr-3',
      style: {
        minWidth: '100px'
      }
    }
  ];
  // Table pagination properties
  const paginationOptions = {
    custom: true,
    totalSize: totalCount,
    sizePerPageList: uiHelpers.sizePerPageList,
    sizePerPage: userUIProps.queryParams.pageSize,
    page: userUIProps.queryParams.pageNumber
  };
  return (
    <>
      <PaginationProvider pagination={paginationFactory(paginationOptions)}>
        {({ paginationProps, paginationTableProps }) => {
          return (
            <Pagination
              isLoading={listLoading}
              paginationProps={paginationProps}
            >
              <BootstrapTable
                wrapperClasses="table-responsive"
                bordered={false}
                classes="table table-head-custom table-vertical-center overflow-hidden"
                bootstrap4
                remote
                keyField="id"
                data={entities === null ? [] : entities}
                columns={columns}
                defaultSorted={uiHelpers.defaultSorted}
                onTableChange={getHandlerTableChange(
                  userUIProps.setQueryParams
                )}
                selectRow={getSelectRow({
                  entities,
                  ids: userUIProps.ids,
                  setIds: userUIProps.setIds
                })}
                {...paginationTableProps}
              >
                <PleaseWaitMessage entities={entities} />
                <NoRecordsFoundMessage entities={entities} />
              </BootstrapTable>
            </Pagination>
          );
        }}
      </PaginationProvider>
    </>
  );
}
