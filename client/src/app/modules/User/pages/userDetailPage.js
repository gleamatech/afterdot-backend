/* eslint-disable jsx-a11y/anchor-is-valid */
import { Field, Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { connect, useDispatch, shallowEqual, useSelector } from 'react-redux';
import { Form } from 'react-bootstrap';
import * as Yup from 'yup';
import * as actions from '../_redux/users/userActions';
import { FormattedMessage, injectIntl } from 'react-intl';
import {
  Card,
  CardBody,
  CardHeader,
  Input
} from '../../../../_metronic/_partials/controls';
import { useHistory } from 'react-router-dom';
import { toast } from 'react-toastify'

function ArticleDetailPage(props) {
  const { intl } = props;
  const dispatch = useDispatch();
  const history = useHistory();
  const [initialValues, setInitialValues] = useState({
    name: '',
    email: '',
    password: '',
    confirmPassword: '',
    role: ''
  });
  const listUser = [
    { title: 'Admin', value: 'admin' },
    { title: 'User', value: 'user' },
    { title: 'Staff', value: 'staff' }
  ];
  // Users Redux state
  const { userForEdit, actionsLoading, user } = useSelector(
    state => ({
      userForEdit: state.users.userForEdit,
      actionsLoading: state.users.actionsLoading,
      user: state.auth.user
    }),
    shallowEqual
  );
  const validateSchema = Yup.object().shape({
    name: Yup.string()
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD'
        })
      ),
    email: Yup.string()
      .email('Wrong email format')
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD'
        })
      ),
    password: Yup.string()
      .min(3, 'Minimum 3 symbols')
      .max(50, 'Maximum 50 symbols')
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD'
        })
      ),
    confirmPassword: Yup.string()
      .required(
        intl.formatMessage({
          id: 'AUTH.VALIDATION.REQUIRED_FIELD'
        })
      )
      .when('password', {
        is: val => (val && val.length > 0 ? true : false),
        then: Yup.string().oneOf(
          [Yup.ref('password')],
          "Password and Confirm Password didn't match"
        )
      })
  });

  const saveUser = async (user) => {
    if (!user.id) {
      const response = await dispatch(actions.createUser(user));
      if(response) {
        toast.success("Tạo user thành công");
        history.push('/user');
      } else  toast.error("Vui lòng kiểm tra lại thông tin đã nhập");
    } else {
      const response = await dispatch(actions.updateUser(user));
      if(response) {
        toast.success("Cập nhật thông tin thành công");
        history.push('/user');
      } else  toast.error("Vui lòng kiểm tra lại thông tin đã nhập");
    }
  };

  useEffect(() => {}, []);

  return (
    <div>
      <Card>
        <CardHeader title={`${userForEdit ? 'Sửa thông tin' : 'Tạo mới'}`} />

        <CardBody>
          {actionsLoading && (
            <div className="overlay-layer bg-transparent">
              <div className="spinner spinner-lg spinner-success" />
            </div>
          )}
          <Formik
            enableReinitialize={true}
            initialValues={userForEdit ? userForEdit : initialValues}
            validationSchema={userForEdit ? false : validateSchema}
            onSubmit={values => {
              saveUser(values);
            }}
          >
            {({ handleSubmit }) => (
              <>
                <Form className="form form-label-right">
                  <div className="form-group row">
                    {userForEdit === undefined && (
                      <>
                        <div className="col-lg-12 mb-8">
                          <Field
                            name="name"
                            component={Input}
                            placeholder="Tên"
                            label="tên"
                          />
                        </div>
                        <div className="col-lg-12 mb-8">
                          <Field
                            name="email"
                            component={Input}
                            placeholder="Email"
                            label="Email"
                          />
                        </div>
                        <div className="col-lg-12 mb-8">
                          <Field
                            name="password"
                            component={Input}
                            placeholder="Mật khẩu"
                            label="mật khẩu"
                          />
                        </div>
                        <div className="col-lg-12 mb-8">
                          <Field
                            name="confirmPassword"
                            component={Input}
                            placeholder="Xác nhận mật khẩu"
                            label="lại mật khẩu"
                          />
                        </div>
                      </>
                    )}
                    {userForEdit && (
                      <div className="col-lg-6">
                        {userForEdit.role === 'admin' ? (
                          <span>
                            Bạn không có quyền thay đổi người dùng này
                          </span>
                        ) : (
                          <>
                            <label htmlFor="">Role</label>
                            <Field
                              as="select"
                              name="role"
                              disable={user.role == 'admin' ? true : false}
                              className="form-control"
                            >
                              {listUser.map((item, index) => (
                                <option value={item.value} key={index}>
                                  {item.title}
                                </option>
                              ))}
                            </Field>
                          </>
                        )}
                      </div>
                    )}
                  </div>
                </Form>

                <button type="button" className="btn btn-light btn-elevate">
                  Hủy
                </button>
                <> </>
                <button
                  type="submit"
                  onClick={() => handleSubmit()}
                  className="btn btn-primary btn-elevate"
                >
                  Đồng Ý
                </button>
              </>
            )}
          </Formik>
        </CardBody>
      </Card>
    </div>
  );
}
export default injectIntl(connect(null, null)(ArticleDetailPage));
