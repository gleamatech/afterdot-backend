import React, { useEffect, useMemo } from 'react';
import { Modal } from 'react-bootstrap';
import { shallowEqual, useDispatch, useSelector } from 'react-redux';
import * as actions from '../../_redux/users/userActions';
import { UserEditDialogHeader } from './UserEditDialogHeader';
import { UserEditForm } from './UserEditForm';
import { useUserUIContext } from '../userUIContext';

export function UserEditDialog({ id, show, onHide }) {
  // Users UI Context
  const UserUIContext = useUserUIContext();
  const UserUIProps = useMemo(() => {
    return {
      initUser: UserUIContext.initUser
    };
  }, [UserUIContext]);

  // Users Redux state
  const dispatch = useDispatch();
  const { actionsLoading, userForEdit } = useSelector(
    state => ({
      actionsLoading: state.hashTags.actionsLoading,
      userForEdit: state.hashTags.userForEdit
    }),
    shallowEqual
  );
  useEffect(() => {
    // server call for getting User by id
    // dispatch(actions.fetchHashTag(id));
  }, [id, dispatch]);

  // server request for saving User
  const saveUser = user => {
    // if (!id) {
    //   // server request for creating User
    //   dispatch(actions.createHashTag(user)).then(() => onHide());
    // } else {
    //   // server request for updating User
    // dispatch(actions.updateHashTag(user)).then(() => onHide());
    // }
  };
  return (
    <Modal
      size="lg"
      show={show}
      onHide={onHide}
      aria-labelledby="example-modal-sizes-title-lg"
    >
      <UserEditDialogHeader id={id} />
      <UserEditForm
        saveUser={saveUser}
        actionsLoading={actionsLoading}
        user={userForEdit || UserUIProps.initHashTag}
        onHide={onHide}
      />
    </Modal>
  );
}
