import { BaseAPI } from '../../../../../redux/setupAxios';

export const USERS_URL = '/api/users';

// CREATE =>  POST: add a new tag to the server
export async function createUser(user) {
  try {
    const { name, email, password, confirmPassword } = user;
    const params = {
      name,
      email,
      password,
      confirmPassword,
    };
    const response = await BaseAPI.post(`${USERS_URL}/register`, params);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log('err', err);
  }
}

// READ
export async function getAllUsers(queryParams) {
  try {
    const valueInputSearch = queryParams.filter.name
      ? queryParams.filter.name
      : false;
    const pageNumber = queryParams.pageNumber;

    if (valueInputSearch) {
      const response = await BaseAPI.post(
        `${USERS_URL}/search?page=${pageNumber}`,
        {
          keyword: valueInputSearch
        }
      );
      return response;
    } else {
      const response = await BaseAPI.get(`${USERS_URL}?page=${pageNumber}`);
      return response;
    }
  } catch (err) {
    console.log(err);
  }
}

export async function getUserById(id) {
  try {
    const response = await BaseAPI.get(`${USERS_URL}/${id}`);
    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

// Method from server should return QueryResultsModel(items: any[], totalsCount: number)
// items => filtered/sorted result
export function findHashTags(queryParams) {
  return BaseAPI.post(`${USERS_URL}/find`, { queryParams });
}

// UPDATE => PUT: update the tags on the server
export async function updateUser(user) {
  try {
    const response = await BaseAPI.put(`${USERS_URL}/${user.id}`, {
      role: user.role
    });

    if (!response.status) return;
    return response;
  } catch (err) {
    console.log(err);
  }
}

// UPDATE Status
export function updateStatusForHashTags(ids, status) {
  return BaseAPI.post(`${USERS_URL}/updateStatusForCustomers`, {
    ids,
    status
  });
}

// DELETE => delete the customer from the server
export async function deleteUser(id) {
  try {
    return await BaseAPI.delete(`${USERS_URL}/${id}`);
  } catch (err) {
    console.log(err);
  }
}

// DELETE tags by ids
export function deleteHashTags(ids) {
  return BaseAPI.post(`${USERS_URL}/deleteHashTags`, { ids });
}
