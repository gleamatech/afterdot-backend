import * as requestFromServer from './userCrud';
import { usersSlice, callTypes } from './userSlice';

const { actions } = usersSlice;

export const fetchHashTags = queryParams => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .findHashTags(queryParams)
    .then(response => {
      const { totalCount, entities } = response.data;
      dispatch(actions.hashTagsFetched({ totalCount, entities }));
    })
    .catch(error => {
      error.clientMessage = "Can't find tag";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchAllUsers = queryParams => dispatch => {
  dispatch(actions.UserFetched({ userForEdit: undefined }));
  dispatch(actions.startCall({ callType: callTypes.list }));
  return requestFromServer
    .getAllUsers(queryParams)
    .then(response => {
      const { data, total } = response;
      dispatch(actions.usersGetAll({ totalCount: total, entities: data.docs }));
    })
    .catch(error => {
      error.clientMessage = "Can't find customers";
      dispatch(actions.catchError({ error, callType: callTypes.list }));
    });
};

export const fetchUser = id => dispatch => {
  if (!id) {
    return dispatch(actions.UserFetched({ userForEdit: undefined }));
  }

  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .getUserById(id)
    .then(response => {
      const user = response.data.doc;
      dispatch(actions.UserFetched({ userForEdit: user }));
    })
    .catch(error => {
      error.clientMessage = "Can't find hash tag";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteUser = id => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteUser(id)
    .then(response => {
      dispatch(actions.userDeleted({ id }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete customer";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const createUser = CreateForCreation => async dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .createUser(CreateForCreation)
    .then(response => {
      dispatch(actions.UserFetched({ userForEdit: undefined }));
      return true
    })
    .catch(error => {
      error.clientMessage = "Can't create customer";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      return false
    });
};

export const updateUser = user => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateUser(user)
    .then(response => {
      dispatch(actions.userUpdated({ user: response.data.doc }));
      return true
    })
    .catch(error => {
      error.clientMessage = "Can't update hashTag";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
      return false
    });
};

export const updateHashTagsStatus = (ids, status) => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .updateStatusForHashTags(ids, status)
    .then(() => {
      dispatch(actions.hashTagsStatusUpdated({ ids, status }));
    })
    .catch(error => {
      error.clientMessage = "Can't update customers status";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};

export const deleteHashTags = ids => dispatch => {
  dispatch(actions.startCall({ callType: callTypes.action }));
  return requestFromServer
    .deleteHashTags(ids)
    .then(response => {
      dispatch(actions.hashTagsDeleted({ ids }));
    })
    .catch(error => {
      error.clientMessage = "Can't delete customers";
      dispatch(actions.catchError({ error, callType: callTypes.action }));
    });
};
