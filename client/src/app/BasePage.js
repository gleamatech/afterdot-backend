import React, { Suspense, lazy } from 'react';
import { Redirect, Switch, Route } from 'react-router-dom';
import { LayoutSplashScreen, ContentRoute } from '../_metronic/layout';
import { BuilderPage } from './pages/BuilderPage';
import { MyPage } from './pages/MyPage';
import { DashboardPage } from './pages/DashboardPage';

const GoogleMaterialPage = lazy(() =>
  import('./modules/GoogleMaterialExamples/GoogleMaterialPage')
);
const ReactBootstrapPage = lazy(() =>
  import('./modules/ReactBootstrapExamples/ReactBootstrapPage')
);
const ECommercePage = lazy(() =>
  import('./modules/ECommerce/pages/eCommercePage')
);
const UserProfilepage = lazy(() =>
  import('./modules/UserProfile/UserProfilePage')
);
const CategoryPage = lazy(() =>
  import('./modules/Category/pages/CategoryPage')
);
const ArticlePage = lazy(() => import('./modules/Article/ArticlePage'));
const ArticleDetailPage = lazy(() => import('./modules/Article/ArticleDetail'));
// const UserPage = lazy(() => import('./modules/User1/UserPage'));
const UserPage = lazy(() => import('./modules/User/pages/userPage'));
const UserDetailPage = lazy(() =>
  import('./modules/User/pages/userDetailPage')
);
const HashTagPage = lazy(() => import('./modules/HashTag/pages/HashTagPage'));
const SettingPage = lazy(() => import('./modules/Setting/SettingPage'));
export default function BasePage() {
  // useEffect(() => {
  //   console.log('Base page');
  // }, []) // [] - is required if you need only one call
  // https://reactjs.org/docs/hooks-reference.html#useeffect

  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/user" />
        }
        <ContentRoute path="/dashboard" component={DashboardPage} />
        <ContentRoute path="/builder" component={BuilderPage} />
        <ContentRoute path="/my-page" component={MyPage} />
        <Route path="/google-material" component={GoogleMaterialPage} />
        <Route path="/react-bootstrap" component={ReactBootstrapPage} />
        <Route path="/e-commerce" component={ECommercePage} />
        <Route path="/user-profile" component={UserProfilepage} />
        <Route path="/article" exact component={ArticlePage} />
        <Route path="/article/detail" component={ArticleDetailPage} />
        <Route path="/user" component={UserPage} />
        <Route path="/user-ce/detail" component={UserDetailPage} />
        <Route path="/category" component={CategoryPage} />
        <Route path="/hash-tag" component={HashTagPage} />
        <Route path="/setting" component={SettingPage} />
        <Redirect to="error/error-v1" />
      </Switch>
    </Suspense>
  );
}
