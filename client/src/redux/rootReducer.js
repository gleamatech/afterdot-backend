// import {all} from "redux-saga/effects";
import { combineReducers } from 'redux';

import * as auth from '../app/modules/Auth/_redux/authRedux';
import { customersSlice } from '../app/modules/ECommerce/_redux/customers/customersSlice';
import { productsSlice } from '../app/modules/ECommerce/_redux/products/productsSlice';
import { remarksSlice } from '../app/modules/ECommerce/_redux/remarks/remarksSlice';
import { specificationsSlice } from '../app/modules/ECommerce/_redux/specifications/specificationsSlice';
import { categorySlice } from '../app/modules/Category/_redux/category/categorySlice';
import { hashTagsSlice } from '../app/modules/HashTag/_redux/hash-tag/hashTagSlice';
import { articlesSlice } from '../app/modules/Article/redux/articlesSlice';
import { usersSlice } from '../app/modules/User/_redux/users/userSlice';

export const rootReducer = combineReducers({
  auth: auth.reducer,
  customers: customersSlice.reducer,
  products: productsSlice.reducer,
  remarks: remarksSlice.reducer,
  specifications: specificationsSlice.reducer,
  categories: categorySlice.reducer,
  hashTags: hashTagsSlice.reducer,
  articles: articlesSlice.reducer,
  users: usersSlice.reducer
});

// export function* rootSaga() {
//   yield all([auth.saga()]);
// }
