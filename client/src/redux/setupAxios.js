import axios from 'axios';
import { actions } from '../app/modules/Auth/_redux/authRedux';
export const BaseAPI = axios.create({
  baseURL: process.env.REACT_APP_API,
  timeout: 12000
});

export default function setupAxios(axios, store) {
  BaseAPI.interceptors.response.use(
    config => {
      const { status } = config.data;
      if (status) {
        return config.data;
      }
      return config;
    },
    error => {
      if(error.response.status === 401){
        store.dispatch(actions.logout());
      }
      Promise.reject(error.response.data);
    }
  );

  BaseAPI.interceptors.request.use(config => {
    const storageAuth=JSON.parse(localStorage.getItem('persist:v713-demo1-auth'))
    const {  auth: { authToken }  } = store.getState();
    if (authToken && storageAuth) {
      config.headers.Authorization = `Bearer ${authToken}`;
    }
    return config;
  });
}
