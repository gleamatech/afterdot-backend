const firebaseAdmin = require('firebase-admin');

const servicesAccount = require('../firebaseTest.json');

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.cert(servicesAccount)
});

const firebaseServices = firebaseAdmin;

module.exports = firebaseServices;
