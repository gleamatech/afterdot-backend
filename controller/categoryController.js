import Category from '../models/categoryModel';
import factories from './factoryHandler';
const catchAsync = require('../utils/catchAsync');
const ApiFeature = require('../utils/apiFeature');

export const getOne = factories.getOne(Category);

export const getAll = factories.getAll(Category);

export const createOne = factories.createOne(Category);

export const updateOne = factories.updateOne(Category);

export const deleteOne = factories.deleteOne(Category);

exports.searchByKeyword = catchAsync(async (req, res, next) => {
  const { keyword, lang } = req.body;

  const f = Category.find({
    $or: [
      { 'name.vi': { $regex: keyword } },
      { 'name.en': { $regex: keyword } }
    ]
  });

  const feature = new ApiFeature(f, req.query).search();

  const total = await feature.query.count();

  const findFeature = new ApiFeature(f, req.query)
    .search()
    .sort()
    .limitFields()
    .pagination();

  const docs = await findFeature.query;

  res.status(200).json({
    status: true,
    results: docs.length,
    data: { docs },
    total,
    page: req.query.page || 1,
    limit: req.query.limit || 10
  });
});
