const fs = require('fs');
const catchAsync = require('../utils/catchAsync');

exports.localUpload = catchAsync(async (req, res, next) => {
  if (!req.file) {
    return res.json({
      status: false,
      msg: 'Vui lòng chọn file để upload'
    });
  }

  const fArray = req.file.originalname.split('.');
  const fileExtension = fArray[fArray.length - 1];
  const image = `${req.file.filename}.${fileExtension}`;
  fs.renameSync(req.file.path, `${req.file.path}.${fileExtension}`);

  return res.json({
    status: true,
    data: `https://afterdot.gleamatech.com/public/uploads/${image}`
  });
});

exports.s3Upload = () => {};

exports.cloudUpdate = () => {};