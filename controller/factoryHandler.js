const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const ApiFeature = require('../utils/apiFeature');
const ApiAdvanced = require('../utils/apiAdvanced');

exports.getAllRef = (Model, refObj) =>
  catchAsync(async (req, res, next) => {
    const docs = await new ApiAdvanced(Model, req.query, refObj)
      .search()
      .sort()
      .pagination()
      .finish();

    res.status(200).json({
      status: true,
      resutls: docs.length,
      data: {
        docs
      }
    });
  });

exports.getAll = (Model, popOpt = null) =>
  catchAsync(async (req, res, next) => {
    console.log(req)
    let f = Model.find();

    // if (popOpt) f = f.populate(popOpt);
    if (popOpt && popOpt.isArray) {
      popOpt.forEach(opt => {
        f = f.populate(opt);
      });
    } else if (popOpt && !popOpt.isArray) {
      f = f.populate(popOpt);
    }

    const feature = new ApiFeature(f, req.query).search();

    const total = await feature.query.count();

    const findFeature = new ApiFeature(f, req.query)
      .search()
      .sort()
      .limitFields()
      .pagination();

    const docs = await findFeature.query;

    res.status(200).json({
      status: true,
      results: docs.length,
      data: { docs },
      total,
      page: req.query.page || 1,
      limit: req.query.limit || 10
    });
  });

exports.getAllByUser = (Model, userField = 'user') =>
  catchAsync(async (req, res, next) => {
    const { user } = req;
    const f = Model.find({ [userField]: user._id });

    const feature = new ApiFeature(f, req.query).search();

    const total = await feature.query.count();

    const findFeature = new ApiFeature(f, req.query)
      .search()
      .sort()
      .limitFields()
      .pagination();

    const docs = await findFeature.query;

    res.status(200).json({
      status: true,
      results: docs.length,
      data: { docs },
      total,
      page: req.query.page || 1,
      limit: req.query.limit || 10
    });
  });

exports.getOne = (Model, popOpt = null) =>
  catchAsync(async (req, res, next) => {
    const { id } = req.params;

    let f = Model.findById(id.toString());

    if (popOpt) f = f.populate(popOpt);

    const doc = await f;
    if (!doc) {
      return next(new AppError('Can not find doc with that Id', 404));
    }

    res.status(200).json({
      status: true,
      data: { doc }
    });
  });

  exports.getOneBySlug = (Model, popOpt = null) =>
  catchAsync(async (req, res, next) => {
    const { slug } = req.params;
    let f = Model.findOne({ slug })

    if (popOpt) f = f.populate(popOpt);

    const doc = await f;
    if (!doc) {
      return next(new AppError('Can not find doc with that Id', 404));
    }

    console.log(doc)

    res.status(200).json({
      status: true,
      data: { doc }
    });
  });

exports.updateOne = Model =>
  catchAsync(async (req, res, next) => {
    const doc = await Model.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });

    res.status(200).json({
      status: true,
      data: { doc }
    });
  });

exports.deleteOne = Model =>
  catchAsync(async (req, res, next) => {
    const doc = await Model.findByIdAndDelete(req.params.id);

    if (!doc) return next(new AppError('Can not find document with Id', 404));

    res.status(200).json({
      status: true,
      data: null
    });
  });

exports.createOne = Model =>
  catchAsync(async (req, res, next) => {
    const doc = await Model.create(req.body);

    res.status(201).json({
      status: true,
      data: { doc }
    });
  });

exports.createByUser = (Model, userField = 'user') =>
  catchAsync(async (req, res, next) => {
    const { user } = req;
    console.log(
      '🚀 ~ file: factoryHandler.js ~ line 142 ~ catchAsync ~ user',
      user
    );

    const doc = await Model.create({ ...req.body, [userField]: user.id });

    res.status(201).json({
      status: true,
      data: { doc }
    });
  });

  exports.searchByKeyword = (Model, field) => catchAsync(async (req, res, next) => { 
    const { keyword } = req.body; 

    const reg = new RegExp(`${keyword}`,'gmi')
   
    const f = Model.find({ [field] : { $regex : reg }}); 
   
    const feature = new ApiFeature(f, req.query).search(); 
   
    const total = await feature.query.count(); 
   
    const findFeature = new ApiFeature(f, req.query) 
      .search() 
      .sort() 
      .limitFields() 
      .pagination(); 
   
    const docs = await findFeature.query; 
   
    res.status(200).json({ 
      status: true, 
      results: docs.length, 
      data: { docs }, 
      total, 
      page: req.query.page || 1, 
      limit: req.query.limit || 10 
    }); 
  });
