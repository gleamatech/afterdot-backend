const _ = require('lodash');
const factory = require('./factoryHandler');
const User = require('../models/userModel');
const catchAsync = require('../utils/catchAsync');
const AppError = require('../utils/appError');

exports.getAllUser = factory.getAll(User);
exports.getUser = factory.getOne(User);
exports.createUser = factory.createOne(User);
exports.deleteUser = factory.deleteOne(User);
exports.updateUser = factory.updateOne(User);
exports.searchByKeyword = factory.searchByKeyword(User, 'name')

exports.setUserId = (req, res, next) => {
  if (!req.body.userId) req.body.userId = req.user._id;
  next();
};

exports.getMe = catchAsync(async (req, res, next) => {
  const me = await User.findById(req.user._id);

  res.status(200).json({
    status: true,
    user: { ..._.omit(me, ['password']) }
  });
});

exports.updateMe = catchAsync(async (req, res, next) => {
  const { name, fmcToken } = req.body;

  const me = await User.findOne({ _id: req.user._id });

  if (name) me.name = name;

  if (fmcToken) me.fmcToken = fmcToken;

  me.save();

  res.status(200).json({
    status: true
  });
});

exports.changeUserPassword = catchAsync(async (req, res, next) => {
  // const { id } = req.body;
  const { user } = req.body;
  const { newPassword } = req.body;

  const u = await User.findById(user._id);

  if (!u) return next(new AppError('Tài khoản không tồn tại', 400));

  // const check = await u.checkPassword(u.password, password);

  // if (!check) return next(new AppError('Mật khẩu không chính xác', 400));

  u.password = newPassword;
  u.passwordConfirm = newPassword;

  await u.save();

  res.json({
    status: true,
    user: u
  });
});

exports.changePassword = catchAsync(async (req, res, next) => {
  const id = _.get(req, ['user', 'id']);

  const { password, newPassword } = req.body;

  const u = await User.findById(id);

  if (!u) return next(new AppError('Email is not exists', 400));

  const check = await u.checkPassword(u.password, password);

  if (!check) return next(new AppError('Mật khẩu không chính xác', 400));

  u.password = newPassword;
  u.passwordConfirm = newPassword;

  await u.save();

  res.json({
    status: true,
    user: u
  });
});
