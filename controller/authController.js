const jwt = require('jsonwebtoken');
const _ = require('lodash');
const crypto = require('crypto');
const { promisify } = require('util');

const google = require('googleapis');
const User = require('../models/userModel');
const AppError = require('../utils/appError');
const catchAsync = require('../utils/catchAsync');
const firebaseServices = require('../services/firebase');
const { resError, resOk } = require('../utils/functions');

const secret = process.env.JWT_SECRET;

const signToken = async userId => {
  return await jwt.sign({ userId }, secret, {
    expiresIn: '90d'
  });
};

const sendToken = async (user, statusCode, res) => {
  const token = await signToken(user._id);

  await User.findByIdAndUpdate(user._id, { token });

  const cookieOpt = {
    expires: new Date(Date.now() + 90 * 24 * 60 * 60 * 1000),
    httpOnly: true
  };

  if (process.env.NODE_ENV === 'production') cookieOpt.secure = true;

  res
    .cookie('jwt', token, cookieOpt)
    .status(statusCode)
    .json({
      status: true,
      token,
      user
    });
};

// Reset User

exports.signUp = catchAsync(async (req, res, next) => {
  let userData = req.body;
  // const agency = _.get(req, 'agency._id');

  userData = _.pick(userData, [
    'name',
    'email',
    'password',
    'passwordConfirm',
    'type'
  ]); //Validate in Mongo

  const user = _.omit(await User.create({ ...userData }), ['password']);

  sendToken(user, 201, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, phoneNumber, password } = req.body;

  const username = phoneNumber ? { phoneNumber } : { email };

  const u = await User.findOne(username).select([
    '-active',
    '-createdAt',
    '+password'
  ]);

  if (!u) return resError('Tài khoản không chính xác', res, 400);

  const check = await u.checkPassword(u.password, password);

  if (!check) return resError('Mật khẩu không chính xác', res, 400);

  sendToken(u, 200, res);
});

exports.loginWithSocial = catchAsync(async (req, res, next) => {
  const { token } = req.body;

  const provider = await firebaseServices.auth().verifyIdToken(token);

  if (provider && provider.email) {
    const userEmail = provider.email

    const user = await User.findOne({
      email: userEmail
    });

    if (!user) {
      const userData = {
        name: provider.name || provider.email,
        email: provider.email,
        password: token.slice(0,50),
        passwordConfirm: token.slice(0,50)
      }
      const newUser = _.omit(await User.create({ ...userData }), ['password']);
      return sendToken(newUser, 201, res);
    }

    return sendToken(user, 200, res);
  }

  return res.json({
    status: false,
    msg: 'Không tìm thấy thông tin vui lòng thử lại!'
  });

});

exports.forgotPassword = catchAsync(async (req, res, next) => {
  const user = await User.findOne({ phoneNumber: req.body.phoneNumber });

  if (!user)
    return next(new AppError('Email that you enter is not exists', 404));

  const resetToken = await user.createResetPasswordToken();
  user.save({ validateBeforeSave: false });

  res.status(200).json({
    status: true,
    message: 'Check your token at email',
    resetToken
  });
});

exports.sendSMS = catchAsync(async (req, res, next) => {
  const { phoneNumber, recaptchaToken } = req.body;
  const user = await User.findOne({ phoneNumber });

  if (!user) {
    return next(new AppError('Không tìm thấy tài khoản trên hệ thống', 404));
  }

  const identityToolkit = google.identitytoolkit({
    auth: 'AIzaSyAWNdt0TWxlzDw658tEgoE-UJS3ZWJ5PY0',
    version: 'v3'
  });

  const response = await identityToolkit.relyingparty.sendVerificationCode({
    phoneNumber,
    recaptchaToken
  });

  // save sessionInfo into db. You will need this to verify the SMS code
  const { sessionInfo } = response.data;

  user.passwordResetToken = sessionInfo;
  user.save();

  res.status(200).json({
    status: true
  });
});

exports.resetByPhone = catchAsync(async (req, res, next) => {
  const { newPassword, resetToken } = req.body;
  const provider = await firebaseServices.auth().verifyIdToken(resetToken);

  if (provider && provider.phone_number) {
    const userNumber = provider.phone_number.replace(/\+84/, '0');

    const user = await User.findOne({
      phoneNumber: userNumber
    });

    user.password = newPassword.toString();
    const d = await user.save();

    return res.status(200).json({
      status: true,
      msg: 'Khôi phục mật khẩu thành công!'
    });
  }

  return res.json({
    status: false,
    msg: 'Không tìm thấy thông tin vui lòng thử lại!'
  });

});

exports.resetPassword = catchAsync(async (req, res, next) => {
  const { resetToken } = req.query;

  const encToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');

  const user = await User.findOne({ passwordResetToken: encToken });

  if (!user) return next(new AppError('Token is not found', 404));

  if (user.passwordResetExpired <= Date.now())
    return next(new AppError('Token is expired', 400));

  const { password, passwordConfirm } = req.body;

  user.password = password;
  user.passwordConfirm = passwordConfirm;
  user.passwordResetToken = undefined;
  user.passwordResetExpired = undefined;

  await user.save();

  sendToken(user, 200, res);
});

//Middleware

exports.protect = catchAsync(async (req, res, next) => {
  const token = req.headers.authorization
    ? req.headers.authorization.split(' ')[1]
    : undefined;

  if (!token)
    return next(new AppError('You have to login to use this function!', 401));

  const jwtVerify = promisify(jwt.verify);

  const verifyJwt = await jwtVerify(token, secret);

  // const user = await User.findById(verifyJwt.userId);

  const user = await User.findOne({ _id: verifyJwt.userId, token });

  if (!user)
    return next(
      new AppError(
        'The user is not exists or no longer exists on our system',
        401
      )
    );

  //Check if password is changed

  if (
    user.passwordChangedAt &&
    parseInt(user.passwordChangedAt.getTime() / 1000, 10) > verifyJwt.iat
  )
    return next(
      new AppError('Please login again, the password has been changed', 401)
    );

  req.user = user;

  next();
});

exports.restrictTo = (...roles) =>
  catchAsync(async (req, res, next) => {
    console.log(roles, req.user.role)
    if (!_.includes(roles, req.user.role))
      return resOk(`You don't have permission to access this page`, res, 400);
    next();
  });
