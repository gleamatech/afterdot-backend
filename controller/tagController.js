import Tag from '../models/tagModel'
import factories from './factoryHandler'
const catchAsync = require('../utils/catchAsync');

export const getOne = factories.getOne(Tag)

export const getAll = factories.getAll(Tag)

// export const createOne = factories.createOne(Tag)

export const updateOne = factories.updateOne(Tag)

export const deleteOne = factories.deleteOne(Tag)

export const searchByKeyword = factories.searchByKeyword(Tag,'name')

exports.createOne = catchAsync(async (req, res, next) => {
    const { name } = req.body;
  
    const isExist = await Tag.findOne({ name });
    if (isExist) {
      return res.status(201).json({
        status: true,
        data: {
          errMes: 'Name is already exists'
        }
      });
    }
  
    const doc = await Tag.create(req.body);
  
    res.status(201).json({
      status: true,
      data: { doc }
    });
});
