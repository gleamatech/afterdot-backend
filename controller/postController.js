import Post from '../models/postModel';
import { resOk } from '../utils/functions';
import factories from './factoryHandler';
const catchAsync = require('../utils/catchAsync');
const ApiFeature = require('../utils/apiFeature');
import slugify from 'slugify';

export const getOne = factories.getOne(Post);

export const getOneBySlug = factories.getOneBySlug(Post);

export const getAll = factories.getAll(Post);

// export const createOne = factories.createOne(Post);

export const updateOne = factories.updateOne(Post);

export const deleteOne = factories.deleteOne(Post);

export const searchByKeyword = factories.searchByKeyword(Post, 'title');

exports.createOne = catchAsync(async (req, res, next) => {
  const { title } = req.body;

  const slug = slugify(title, {
    replacement: '-',
    lower: true,
    strict: true,
    locale: 'vi'
  });

  const isExist = await Post.findOne({ slug });
  if (isExist) {
    return res.status(201).json({
      status: true,
      data: {
        errMes: 'Slug is already exists'
      }
    });
  }

  const doc = await Post.create(req.body);

  res.status(201).json({
    status: true,
    data: { doc }
  });
});

exports.searchByCategory = catchAsync(async (req, res, next) => {
  const { category } = req.body;

  const f = Post.find({ category: { $in: category } });

  const feature = new ApiFeature(f, req.query).search();

  const total = await feature.query.count();

  const findFeature = new ApiFeature(f, req.query)
    .search()
    .sort()
    .limitFields()
    .pagination();

  const docs = await findFeature.query;

  res.status(200).json({
    status: true,
    results: docs.length,
    data: { docs },
    total,
    page: req.query.page || 1,
    limit: req.query.limit || 10
  });
});

exports.searchByTag = catchAsync(async (req, res, next) => {
  const { tag } = req.body;

  const f = Post.find({ tags: { $in: tag } });

  const feature = new ApiFeature(f, req.query).search();

  const total = await feature.query.count();

  const findFeature = new ApiFeature(f, req.query)
    .search()
    .sort()
    .limitFields()
    .pagination();

  const docs = await findFeature.query;

  res.status(200).json({
    status: true,
    results: docs.length,
    data: { docs },
    total,
    page: req.query.page || 1,
    limit: req.query.limit || 10
  });
});
