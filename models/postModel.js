import slugify from 'slugify';

const mongoose = require('mongoose');
const mgAuto = require('mongoose-autopopulate')
const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    slug: {
      type: String,
      required: false,
    },
    content: String,
    category: {
      type: mongoose.Schema.Types.Array,
      required: [true, 'category is requied'],
      ref: 'Category',
      autopopulate: true
    },
    tags: {
      type: [String],
      required: true,
      ref: 'Tag'
      // autopopulate: true
    },
    image:{
      type: String,
      required: true
    },
    traffic: {
      type: Number,
      "default": 0
    },
    createdAt: {
      type: Date,
      default: Date.now
    },
    updatedAt: {
      type: Date,
      default: Date.now
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false
  }
);

postSchema.pre(/^save/, function (next) {
  this.slug = slugify(this.title, {
    replacement: '-',
    lower: true,
    strict: true,
    locale: 'vi'
  })
  next();
})

// postSchema.pre(/^find/, function (next) {
//   console.log(this)
//   next();
// })

postSchema.plugin(mgAuto)

const Post = mongoose.model('Post', postSchema);

export default Post
