const mongoose = require('mongoose');

const configSchema = new mongoose.Schema(
  {
    configName: String,
    configValue: String
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false
  }
);

const Config = mongoose.model('User', configSchema);

export default Config