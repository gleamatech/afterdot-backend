const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema(
  {
    name: {
      vi:{
        type: String,
        required: true,
        unique: true
      },
      en: {
        type: String,
        required: true,
        unique: true
      }
    },
    createdAt: {
      type: Date,
      default: Date.now
    }
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false
  }
);

const Category = mongoose.model('Category', categorySchema);

export default Category
