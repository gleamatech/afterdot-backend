const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const crypto = require('crypto')
const beautifyUnique = require('mongoose-beautiful-unique-validation')

const userSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: [true, 'Vui lòng nhập tên để đăng ký']
    },
    email: {
      type: String,
      lowercase: true,
      validate: {
        validator: validator.isEmail,
        message: 'You must enter correct email format'
      },
      unique: 'Email đã được đăng ký, vui lòng thử lại',
      default: `${new Date().getTime()}@gmail.com`
    },
    password: {
      type: String,
      required: [true, 'Password is required'],
      min: 8
    },
    passwordConfirm: {
      type: String
    },
    role: {
      type: String,
      enum: ['admin', 'user', 'vip', 'seller', 'staff'],
      default: 'user'
    },
    type:{
      type: String,
      default: 'normal'
    },
    balance: {
      type: Number,
      default: 0
    },
    image: String,
    address: String,
    country: String,
    passwordChangedAt: Date,
    passwordResetToken: String,
    passwordResetExpired: Date,
    createdAt: {
      type: Date,
      default: Date.now()
    },
    active: {
      type: Boolean,
      default: true
    },
    token: String,
    fmcToken: String
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false
  }
)

userSchema.plugin(beautifyUnique)

userSchema.pre('save', async function (next) {
  if (!this.isModified('password')) return next()

  // hash password
  this.password = await bcrypt.hash(this.password, 12)
  // delete password confirm after has
  this.passwordConfirm = undefined

  if (!this.isNew) this.passwordChangedAt = Date.now()

  next()
})

userSchema.pre('save', function (next) {
  if (!this.isModified('password') || this.isNew) return next()

  this.passwordChangedAt = Date.now()

  next()
})

userSchema.methods.checkPassword = async function (password, inputPassword) {
  return await bcrypt.compare(inputPassword, password)
}

userSchema.methods.createResetPasswordToken = async function () {
  const token = crypto.randomBytes(32).toString('hex')
  this.passwordResetToken = crypto
    .createHash('sha256')
    .update(token)
    .digest('hex')
  this.passwordResetExpired = Date.now() + 15 * 60 * 1000
  return token
}

const User = mongoose.model('User', userSchema)

module.exports = User
