const express = require('express');

const router = express.Router();

const userController = require('../controller/userController');

const authController = require('../controller/authController');

//Auth

router.post('/login', authController.login);
router.post('/register', authController.signUp);
router.post('/forgotpassword', authController.forgotPassword);
router.post('/reset', authController.resetByPhone);
router.patch('/resetPassword', authController.resetPassword);
router.post('/loginWithSocial', authController.loginWithSocial)

// Updated

// router.route('/sendSMS').get(authController.sendSMS);

// router.route('/resetPassword').get(authController.resetByPhone);

router.use(authController.protect);

router.get('/getme', userController.getMe);

router.post('/changePassword', userController.changePassword);

router.use(authController.restrictTo('admin','staff'));

router.route('/').get(userController.getAllUser)

router.route('/:id').get(userController.getUser)

router.route('/search').post(userController.searchByKeyword);

router.use(authController.restrictTo('admin'));

router
  .route('/:id')
  .put(userController.updateUser)
  .delete(userController.deleteUser);

module.exports = router;
