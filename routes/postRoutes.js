import * as postController from '../controller/postController';
import { authenticate } from '../utils/functions';
import { restrictTo } from '../controller/authController';

const express = require('express');

const router = express.Router();

router.route('/').get(postController.getAll);

router.route('/:id').get(postController.getOne);

router.route('/slug/:slug').get(postController.getOneBySlug);

router.route('/category').post(postController.searchByCategory);

router.route('/tag').post(postController.searchByTag);

router.route('/search').post(postController.searchByKeyword);

router.use(authenticate());

router.use(restrictTo('admin', 'staff'));

router.route('/').post(postController.createOne);

router
  .route('/:id')
  .put(postController.updateOne)
  .delete(postController.deleteOne);

module.exports = router;
