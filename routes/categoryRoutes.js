import { restrictTo } from '../controller/authController';
import * as categoryController from '../controller/categoryController';
import { authenticate } from '../utils/functions';
const express = require('express');
const router = express.Router();


router
  .route('/')
  .get(categoryController.getAll)

router
  .route('/:id')
  .get(categoryController.getOne)

router.use(authenticate());

router.use(restrictTo('admin', 'staff'))

router.route('/').post(categoryController.createOne);

router.route('/search').post(categoryController.searchByKeyword)

router
  .route('/:id')
  .put(categoryController.updateOne)
  .delete(categoryController.deleteOne);


module.exports = router;
