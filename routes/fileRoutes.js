const express = require('express');
const multer = require('multer');

const authController = require('../controller/authController');

const fileController = require('../controller/fileController')

const upload = multer({ dest: 'public/uploads'});
const router = express.Router();

router.use(authController.protect);

router.use(authController.restrictTo('admin','staff'));

router.route('/').post(upload.single('image'), fileController.localUpload), 


module.exports = router;
