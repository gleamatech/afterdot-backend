const express = require('express');

const router = express.Router();
const userRoutes = require('./userRoutes');
const postRoutes = require('./postRoutes');
const categoryRoutes = require('./categoryRoutes');
const tagRoutes = require('./tagRoutes');
const fileRotes = require('./fileRoutes')

//Routes use
router.use('/users', userRoutes);
router.use('/posts', postRoutes);
router.use('/categories', categoryRoutes)
router.use('/tags', tagRoutes)
router.use('/file', fileRotes)


module.exports = router;
