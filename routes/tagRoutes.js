import { restrictTo } from '../controller/authController';
import * as tagController from '../controller/tagController';
import { authenticate } from '../utils/functions';
const express = require('express');
const router = express.Router();


router
  .route('/')
  .get(tagController.getAll)

router
  .route('/:id')
  .get(tagController.getOne)

router.use(authenticate());

router.use(restrictTo('admin', 'staff'))

router.route('/').post(tagController.createOne);

router.route('/search').post(tagController.searchByKeyword);

router
  .route('/:id')
  .put(tagController.updateOne)
  .delete(tagController.deleteOne);

module.exports = router;
